# Team10-Quiz-Project

## 1. Project Overview
The Quiz Platform is a Singe-page application for creating and taking quizzes. It has two main parts: 

1. **Students**, attempt quizzes in different categories, get scores and try to make it to the leaderboard of top 10 students. 
2. **Teachers**, can create quizzes, view students' performance by quiz and simulate taking a quiz and getting a score. 

## 2. Main Technologies:
JavaScript, MariaDB, MySQL, Node.js, ExpressJS, JWT, Git, React, Redux, Bootstrap

## 3. Prerequisites for Starting the Project:
- Node.js
- VScode
- Git
- Maria DB (download - https://mariadb.org/download/) - configure and remember your password for the root user
- MySQL WorkBench (download - https://dev.mysql.com/downloads/workbench/)

## 4. Setting Up the Server and DataBase of the Project
  1. Clone this Repository locally and pull it's contents
  2. Open the ```\api``` folder in VScode and in the terminal run ```npm install```
  3. Open the ```\api\src\config.js``` file and chage the ```DB_CONFIG``` password to your Maria DB root user password
  4. DB: Open MySQL Workbench -> File -> Open SQL Script -> open ```quizdb1.sql``` and run the script; refresh the schemas to see the new Database 
      - Note this is a demo DB; for an empty DB repeat the same step with file ```quizdbnew.sql``` and change the database property of ```DB_CONFIG``` in ```\api\src\config.js``` to "quizdbnew"
  5. In VScode's terminal open on ```\api``` run ```npm run start:dev``` to serve the app; You should see this ``Listening on port 4000`` in the terminal if succesful;

## 5. Setting Up The React App (Frontend)
  1. Open the ```\frontend``` folder in VScode and in the terminal run ```npm install```
  2. To start the app run ```npm start``` - within a few seconds you should see the app open a new tab in your browser

## 7. Starting with Demo DB:
Once you have the Home page open you can sing-in or sign-up. Here are the main options:

- You can sign-Up as a new teacher at a secret url: ```http://localhost:3000/register/only-for-teachers```
- You can sign-Up as a new student by Clicking `Create Account` 

Once you are at the dashboard page you can explore all the features of the app.
Please note:
- Teachers can take a quiz as many times as they want
- Students are allowed only one attempt at a quiz and can solve only one quiz at a time
## 6. Starting with new DB: 
Don't forget to first:  
- DB: Open **MySQL Workbench** -> File -> Open SQL Script -> open ```quizdbnew.sql``` run the script
- in ```\api\src\config.js```change the database property of ```DB_CONFIG```  to "quizdbnew",  ; refresh the schemas to see the new Database 

Sign up as a new teacher at a secret url: ```http://localhost:3000/register/only-for-teachers```

Since there are no quizzes nor categories you will need to:
1. Create a Quiz Category(ies)
2. Start creating quizzes

- Teachers can take a quiz as many times as they want
- Students are allowed only one attempt at a quiz and can solve only one quiz at a time