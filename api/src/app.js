import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { PORT } from './config.js';
import passport from 'passport';
import teacherController from './controllers/teacher-controller.js';
import studentController from './controllers/student-controller.js';
import authController from './controllers/auth-controller.js';
import jwtStrategy from './auth/strategy.js';
import { authMiddleware, roleMiddleware, tokenMiddleware } from './auth/auth-middleware.js';

const app = express();
passport.use(jwtStrategy);

app.use(cors(), bodyParser.json());
app.use(passport.initialize());

app.use('/auth', authController);
app.use('/api', studentController);
app.use(
  '/api/teacher',
  authMiddleware,
  roleMiddleware('teacher'),
  tokenMiddleware(),
  teacherController
);
app.use('/public', express.static('images'));

app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found!' })
);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
