import serviceErrors from '../service-errors/service-errors.js';
import bcrypt from 'bcrypt';

const signUp = (usersData) => {
  return async (userCreate) => {
    const { username, password, firstName, lastName, role } = userCreate;
    const existingUser = await usersData.getBy('username', username);

    if (existingUser) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }

    const passwordHash = await bcrypt.hash(password, 10);

    if (role === 'teacher') {
      const user = await usersData.createTeacher(
        username.toLowerCase(),
        passwordHash,
        firstName,
        lastName
      );
      return { error: null, user };
    } else {
      const user = await usersData.createStudent(
        username.toLowerCase(),
        passwordHash,
        firstName,
        lastName
      );
      return { error: null, user };
    }
  };
};

const signIn = (usersData) => {
  return async (username, password) => {
    const user = await usersData.getUserFullInfo('username', username);

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    return {
      error: null,
      user: user,
    };
  };
};

const signOut = (usersData) => {
  return async (token) => {
    const blackListedToken = await usersData.getToken(token);

    if (blackListedToken[0]) {
      return blackListedToken[0];
    }

    const newToken = await usersData.addToken(token);
    return newToken[0];
  };
};


const updateAvatar = usersData => {
  return async (id, path) => {
    const result = await usersData.updateAvatar(id, path);

    return { error: result.affectedRows > 0 ? null : serviceErrors.UNSPECIFIED_ERROR };
  }
};

const getUsersInfo = usersData => {
  return async (id) => {
    const result = await usersData.getUserFullInfo('id', id);

    return { result };
  }
};

const getUsersActiveQuiz = usersData => {
  return async (id) => {
    const result = await usersData.getActiveQuiz(id);

    return { result };
  }
};

export default {
  signUp,
  signIn,
  signOut,
  updateAvatar,
  getUsersInfo,
  getUsersActiveQuiz
};
