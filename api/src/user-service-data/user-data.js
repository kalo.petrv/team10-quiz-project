import pool from '../pool.js';

const getBy = async (column, username) => {
    const sql = `
      SELECT id, username, firstname, lastname, role, active_quiz
      FROM users
      WHERE ${column} = '${username}';
    `;

    const result = await pool.query(sql);
    return result[0];
};

const createStudent = async (username, password, firstName, lastName) => {
    const sql = `
    INSERT INTO users (username, password, firstname, lastname)
    VALUES (?, ?, ?, ?);
    `;

    const result = await pool.query(sql, [username, password, firstName, lastName]);
    return {
        id: result.insertId,
        username: username,
        firstName: firstName,
        lastName: lastName
    };
}

const createTeacher = async (username, password, firstName, lastName) => {
    const sql = `
    INSERT INTO users (username, password, firstname, lastname, role)
    VALUES (?, ?, ?, ?, ?);
    `;

    const result = await pool.query(sql, [username, password, firstName, lastName, 'teacher']);
    return {
        id: result.insertId,
        username: username,
        firstName: firstName,
        lastName: lastName
    };
}

const getUserFullInfo = async (column, username) => {
    const sql = `
      SELECT *
      FROM users
      WHERE ${column} = '${username}'
    `;

    const result = await pool.query(sql);
    return result[0];
}

const getToken = async (token) => {
    const sql = `
    SELECT *
    FROM token_blacklist 
    WHERE token = ?;`;

    return pool.query(sql, [token]);
}

const addToken = async (token) => {
    const sql = ` INSERT INTO token_blacklist(token)
    VALUES (?)`;

    return pool.query(sql, [token]);
}

const updateAvatar = async (id, path) => {
    const sql = `
        UPDATE users SET
        avatar = ?
        WHERE id = ?
    `;

    return await pool.query(sql, [path, id]);
}

const setActiveQuiz = async (value, user_id) =>{
    const sql=`
      UPDATE users
      SET active_quiz =  ${value} 
      WHERE (id = ${user_id});
    `;

    return await pool.query(sql);
}


const getActiveQuiz = async (user_id) =>{
    const sql=`
        SELECT active_quiz
        FROM users
        WHERE id = ${user_id};
    `;

    const result =  await pool.query(sql);
    return result[0];
}

export default {
    getBy,
    createStudent,
    createTeacher,
    getUserFullInfo,
    getToken,
    addToken,
    updateAvatar,
    getActiveQuiz,
    setActiveQuiz
}
