import passport from 'passport';
import usersData from '../user-service-data/user-data.js';

const authMiddleware = passport.authenticate('jwt', { session: false });

const roleMiddleware = (roleName) => {
    return (req, res, next) => {
        if (req.user && req.user.role === roleName) {
            next()
        } else {
            res.status(403).send({
                error: 'Resource is forbidden.'
            })
        };
    };
};

const tokenMiddleware = () => {
    return async (req, res, next) => {
        const isTokenBlacklisted = await usersData.getToken(req.headers.authorization);
        if (isTokenBlacklisted[0]) {
            res.status(400).send({ error: 'Invalid Session' })
        } else {
            next()
        };
    };
};

export {
    authMiddleware,
    roleMiddleware,
    tokenMiddleware
};
