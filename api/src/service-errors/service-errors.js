export default {
    /** 1 Such a record does not exist (when it is expected to exist) */
    RECORD_NOT_FOUND: 1,
    /** 2 The requirements do not allow more than one of that resource */
    DUPLICATE_RECORD: 2,
    /** 3 The requirements do not allow such an operation */
    OPERATION_NOT_PERMITTED: 3,
    /** 4 username/password mismatch */
    INVALID_SIGNIN: 4,

    UNSPECIFIED_ERROR: 5,

    OPERATION_NOT_PERMITTED: 6


}
