export const createQuizSchema = {
  quiz: (value) => {
    if (!value?.quiz_details?.name.trim()) {
      return 'Quiz Name is required';
    }

    if (
      typeof value?.quiz_details?.name !== 'string' ||
      value?.quiz_details?.name.length < 5 ||
      value?.quiz_details?.name.length > 30
    ) {
      return 'Quiz Name should be a string in range [5..30]';
    }

    if (!value?.quiz_details?.category_name) {
      return 'Category is required';
    }

    const questions = value?.quiz_details?.questions;

    if (questions.length < 2) {
      return 'Quiz should have at least 2 questions'
    }

    for (let i = 0; i < questions.length; i++) {
      if (!questions[i]?.question || questions[i].question.trim().length < 3) {

        return `Question ${i + 1} is required & should be in range [3..30]`;
      }

      if (questions[i]?.points < 1 || questions[i]?.points > 6) {
        return `Question ${i + 1} requires points selection`;
      }

      let correctAnswers = 0;

      if (questions[i]?.answers.length < 2) {
        return `Question ${i + 1} requires at least 2 possible answers`
      }

      for (let j = 0; j < questions[i]?.answers.length; j++) {
        if (!questions[i]?.answers[j]?.answer) {
          return `Question ${i + 1} - answer ${j + 1} is required`;
        }

        if (questions[i]?.answers[j]?.is_correct === 1) {
          correctAnswers += 1;
        }
      }

      if (correctAnswers < 1) {
        return `Question ${i + 1} requires at least 1 correct answer`;
      }
    }

    return null;
  },
};
