export const createCategorySchema = {
    category_name: value => {
        if (!value) {
            return 'Category is required';
        }
        
        
        if (typeof value !== 'string' || value.length < 2 || value.length > 30) {
            return 'Category should be a string in range [2..30]';
        }

        return null;
    },
};
