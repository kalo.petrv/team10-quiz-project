import categoriesData from '../categories-service-data/categories-data.js';
import serviceErrors from '../service-errors/service-errors.js';
import ServiceErrors from '../service-errors/service-errors.js';
import usersData from '../user-service-data/user-data.js';

const getQuizzesByTeacher = (quizzesData) => {
  return async (user_id) => {
    return await quizzesData.getLast5QuizzesByTeacher(user_id);
  };
};

/**
 *
 * @param {*} quizzesData
 * @returns {Object} All quizzes
 * Displays all quizzes
 *
 * @function getAllQuizzes
 * @param {object} quizzesData The object which properties are functions to use
 * @return
 * @async
 * @function
 * @param {string} filter The name or part of it
 * @return {Promise<object>} The data from query or error
 */
const getQuizByID = (quizzesData) => {
  return async (id, user_id, category_id) => {
    const checkUserRole = await usersData.getBy('id', user_id);

    if (checkUserRole.role === 'student') {
      const checkIfUserHasDoneQuiz = await quizzesData.checkIfUserDoneQuiz(
        user_id,
        id,
        category_id
      );

      if (checkUserRole.active_quiz !== 0 && checkUserRole.active_quiz !== id) {
        return {
          error: ServiceErrors.UNSPECIFIED_ERROR,
          score: null,
        };
      }

      if (checkIfUserHasDoneQuiz[0]) {
        return {
          error: ServiceErrors.OPERATION_NOT_PERMITTED,
          score: null,
        };
      }
    }

    const quiz = await quizzesData.getQuizDetailsById(id);
    const questions = await quizzesData.getQuestionsByQuizID(id);
    const answers = await quizzesData.getAnswersByQuizID(id);

    if (!quiz[0]) {
      return {
        error: ServiceErrors.RECORD_NOT_FOUND,
        quiz: null,
      };
    }

    const questionsWithAnswers = questions.reduce((ac, q) => {
      q.answers = answers.reduce((acc, a) => {
        if (q.id === a.question_id) {
          acc.push(a);
        }
        return acc;
      }, []);
      ac.push(q);
      return ac;
    }, []);

    const setActive = usersData.setActiveQuiz(id, user_id);

    return {
      error: null,
      quiz: {
        quiz_details: quiz,
        questions: questionsWithAnswers,
      },
    };
  };
};

const createQuiz = (quizzesData) => {
  return async (quiz, user_id) => {
    const { name, questions, category_name } = quiz;
    const category = await categoriesData.getBy('category_name', category_name);

    const existingQuiz = await quizzesData.searchQuiz(name);

    if (existingQuiz[0]) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        newCategory: null,
      };
    } else {
      const newQuiz = await quizzesData.createNewQuiz(
        name,
        category[0].id,
        user_id
      );

      const createdQuestions = await quizzesData.createQuestionsAndAnswers(
        questions,
        newQuiz.id,
        category[0].id
      );

      return {
        error: null,
        newQuiz: await quizzesData.searchQuiz(name),
      };
    }
  };
};

const getQuizzesByCategoryID = (quizzesData) => {
  return async (id) => {
    const quizzes = await quizzesData.getByCategoryId(id);

    if (!quizzes) {
      return {
        quizzes: null,
        error: ServiceErrors.RECORD_NOT_FOUND,
      };
    }

    return {
      quizzes: quizzes,
      error: null,
    };
  };
};

const getQuizzesByCategoryIDStudent = (quizzesData) => {
  return async (id, user_id) => {
    const quizzes = await quizzesData.getByCategoryIdStudent(id, user_id);

    if (!quizzes || quizzes.length === 0) {
      return {
        quizzes: null,
        error: ServiceErrors.RECORD_NOT_FOUND,
      };
    }

    return {
      quizzes: quizzes,
      error: null,
    };
  };
};

const submitQuiz = (quizzesData) => {
  return async (category_id, quiz_id, answers, user_id) => {
    const quizInfo = await quizzesData.getQuizWithCorrectAnswers(
      +category_id,
      +quiz_id
    );
    const check = await quizzesData.checkIfUserDoneQuiz(
      user_id,
      quiz_id,
      category_id
    );
    const checkUserRole = await usersData.getBy('id', user_id);
    let score = 0;

    if (!quizInfo[0]) {
      return {
        error: ServiceErrors.RECORD_NOT_FOUND,
        score: null,
      };
    }

    if (check[0]) {
      return {
        error: ServiceErrors.OPERATION_NOT_PERMITTED,
        score: null,
      };
    }
    const resultsAboutUserAnswers = quizInfo.map((q) => {
      return answers.answers.reduce((acc, a) => {
        if (q.question_id === a.question_id) {
          const isAnswerCorrect =
            a.answers_IDS.sort((a, b) => a - b).join(',') === q.answers_id;
          if (isAnswerCorrect) {
            score += +q.points;
          }
          Object.assign(acc, {
            student_score: isAnswerCorrect ? +q.points : 0,
            date_taken: Date.now(),
            question_id: +q.question_id,
            quiz_id: +quiz_id,
            category_id: +category_id,
            users_id: +user_id,
          });
        }
        return acc;
      }, {});
    });

    if (checkUserRole.role === 'student') {
      const setActive = usersData.setActiveQuiz(0, user_id);
      const submitAnswers = await quizzesData.submitUserAnswers(
        resultsAboutUserAnswers
      );
    }

    const actualQuiz = await quizzesData.getQuizDetailsById(quiz_id);

    return {
      error: null,
      score: score,
      possiblePoints: actualQuiz[0].possiblePoints,
    };
  };
};

const getPointsForQuiz = (quizzesData) => {
  return async (quizId) => {
    const points = await quizzesData.getPointsForQuizByID(quizId);
    return points;
  }
}

const getScore = (quizzesData) => {
  return async (category_id, user_id, quiz_id) => {
    const checkIsValidQuiz = await quizzesData.getQuizByCategoryIdAndQuizId(
      category_id,
      quiz_id
    );

    const checkIsUserDoneQuiz = await quizzesData.checkIfUserDoneQuiz(
      +user_id,
      +quiz_id
    );

    if (!checkIsValidQuiz) {
      return {
        error: ServiceErrors.RECORD_NOT_FOUND,
        scores: null,
      };
    }

    if (!checkIsUserDoneQuiz) {
      return {
        error: ServiceErrors.OPERATION_NOT_PERMITTED,
        scores: null,
      };
    }

    const scores = await quizzesData.getScoresFromQuizByUserIdAndQuizId(
      +user_id,
      +quiz_id
    );

    return {
      error: null,
      scores: scores,
    };
  };
};

const getStudentHistory = (quizzesData) => {
  return async (user_id) => {
    const history = await quizzesData.getHistory(user_id);

    if (!history[0]) {
      return {
        error: ServiceErrors.RECORD_NOT_FOUND,
        history: null,
      };
    } else {
      return {
        error: null,
        history: history,
      };
    }
  };
};

const getStudentHistoryLastFive = (quizzesData) => {
  return async (user_id) => {
    const history = await quizzesData.getLastFiveFromHistory(user_id);

    if (!history[0]) {
      return {
        error: ServiceErrors.RECORD_NOT_FOUND,
        history: null,
      };
    } else {
      return {
        error: null,
        history: history,
      };
    }
  };
};

const getStudentHistoryByCategory = (quizzesData) => {
  return async (user_id, category_id) => {
    const history = await quizzesData.getHistoryByCategory(
      user_id,
      category_id
    );

    if (!history[0]) {
      return {
        error: ServiceErrors.RECORD_NOT_FOUND,
        history: null,
      };
    } else {
      return {
        error: null,
        history: history,
      };
    }
  };
};

const getAllStudentsScoresForQuiz = (quizzesData) => {
  return async (quiz_id) => {
    const allStudentScores = await quizzesData.getStudentScoresByQuizId(
      quiz_id
    );

    return {
      error: null,
      scores: allStudentScores,
    };
  };
};


export default {
  createQuiz,
  getQuizByID,
  getQuizzesByCategoryID,
  getQuizzesByCategoryIDStudent,
  submitQuiz,
  getScore,
  getStudentHistory,
  getStudentHistoryLastFive,
  getStudentHistoryByCategory,
  getAllStudentsScoresForQuiz,
  getQuizzesByTeacher,
  getPointsForQuiz
};
