import pool from '../pool.js';

const getLast5QuizzesByTeacher = async (user_id) => {
  const sql = `
    SELECT q.id, q.quiz_name, c.category_name, c.id as category_id, COUNT(qs.question) 
    as questions, SUM(qs.points) as possiblePoints 
    FROM quizzes q
    JOIN categories c ON c.id = q.category_id
    JOIN questions qs ON qs.quiz_id = q.id
    WHERE users_id = ${user_id}
    GROUP BY qs.quiz_id
    ORDER BY qs.id desc
    LIMIT 5;
    `;
  return await pool.query(sql);
};

const getQuizDetailsById = async (id) => {
  const sql = `
      SELECT q.id, q.quiz_name, c.category_name, COUNT(qs.question) 
      as questions, SUM(qs.points) as possiblePoints 
      FROM quizzes q
      JOIN categories c ON c.id = q.category_id
      JOIN questions qs ON qs.quiz_id = q.id
      WHERE q.id = ${id}
      GROUP BY qs.quiz_id;
      `;
  return await pool.query(sql);
};

const getQuestionsByQuizID = async (id) => {
  const sql = `
    SELECT qs.id, qs.question, qs.points, qs.q_type as type FROM quizzes q
    JOIN questions qs ON q.id = qs.quiz_id
    WHERE q.id = ${id}; 
    `;
  return await pool.query(sql);
};

const getByCategoryId = async (id) => {
  const sql = `
    SELECT q.id, q.quiz_name, c.category_name, COUNT(qs.question) 
    as questions, SUM(qs.points) as possiblePoints 
    FROM quizzes q
    JOIN categories c ON c.id = q.category_id
    JOIN questions qs ON qs.quiz_id = q.id
    WHERE q.category_id = ${id}
    GROUP BY qs.quiz_id;
  `;
  return await pool.query(sql);
};

const getByCategoryIdStudent = async (category_id, user_id) => {
  const sql = `
    SELECT q.id, q.quiz_name, c.category_name, COUNT(qs.question) 
    as questions, SUM(qs.points) as possiblePoints 
    FROM quizzes q
    JOIN categories c ON c.id = q.category_id
    JOIN questions qs ON qs.quiz_id = q.id
    WHERE q.category_id = ${category_id} and 
    q.id NOT IN ( select DISTINCT quiz_id from scores_per_question
    where users_id = ${user_id} )
    GROUP BY qs.quiz_id;
  `;
  return await pool.query(sql);
}

const getAnswersByQuizID = async (id) => {
  const sql = `
    SELECT a.id, a.answer, a.is_correct, qs.id as question_id FROM quizzes q
    JOIN questions qs ON q.id = qs.quiz_id
    JOIN answers a ON qs.id = a.question_id
    WHERE q.id = ${id}; 
    `;
  return await pool.query(sql);
};

const createNewQuiz = async (quiz_name, category_id, users_id) => {
  const sql = `
    INSERT INTO quizzes(quiz_name, category_id, users_id)
    VALUES(?, ?, ?)
    `;
  const result = await pool.query(sql, [quiz_name, category_id, users_id]);

  return {
    id: result.insertId,
  };
};

const createQuestionsAndAnswers = async (questions, quiz_id, category_id) => {
  const questionsToInsert = questions.reduce((acc, q) => {
    const stringifiedQuestion = q.question.split('').map(el => {
      if (el === '"') {
        el = "'"
      } 
      return el;
    }).join('')

    acc += `
    ("${stringifiedQuestion}", ${q.points}, ${quiz_id}, ${category_id}, ${q.type}),`;
    return acc;
  }, ``);

  const sql = `
    INSERT INTO questions(question, points, quiz_id, category_id, q_type)
    VALUES${questionsToInsert.slice(0, -1)};
    `;
  const result = await pool.query(sql);
  let questionId = result.insertId;

  const answersToInsert = questions.reduce((acc, q) => {
    q.answers.forEach((a) => {
      const stringifiedAnswer = a.answer.split('').map(el => {
        if (el === '"') {
          el = "'"
        } 
        return el;
      }).join('')

      acc += `
    ("${stringifiedAnswer}", ${a.is_correct}, ${questionId}, ${quiz_id}, ${category_id}),`;
    });
    questionId++;
    return acc;
  }, ``);

  const sql2 = `
  INSERT INTO answers(answer, is_correct, question_id, quiz_id, category_id)
  VALUES${answersToInsert.slice(0, -1)};
  `;
  const result2 = await pool.query(sql2);

  return {
    id: result2.insertId,
  };
};

const submitAnswers = async (
  student_score,
  date_taken,
  question_id,
  quiz_id,
  category_id,
  users_id
) => {
  const sql = `
    INSERT INTO scores_per_question (student_score, date_taken, question_id, quiz_id, category_id, users_id) 
    VALUES (?, ?, ?, ?, ?, ?);
  `;
  return await pool.query(sql, [
    student_score,
    date_taken,
    question_id,
    quiz_id,
    category_id,
    users_id,
  ]);
};

const getQuizByQuizId = async (quiz_id, category_id) => {
  const sql = `
    SELECT * FROM quizzes
    WHERE id = ${quiz_id} AND category_id = ${category_id};
  `;

  const result = await pool.query(sql);
  return result;
};

const checkIfUserDoneQuiz = async (user_id, quiz_id, category_id) => {
  const sql = `
    SELECT * FROM scores_per_question 
    WHERE users_id = ${user_id} AND quiz_id = ${quiz_id} AND category_id = ${category_id};
  `;

  return await pool.query(sql);
};

const getScoresFromQuizByUserIdAndQuizId = async (user_id, quiz_id) => {

  const sql = `
    SELECT u.username, SUM(spq.student_score) AS score
    FROM scores_per_question spq
    JOIN users u
    ON u.id = spq.users_id
    WHERE spq.quiz_id = ${quiz_id} 
    AND spq.users_id = ${user_id};
  `;

  const result =  await pool.query(sql);
  return result[0]
};

const getHistory = async (user_id) => {
  const sql = `
    SELECT distinct sq.quiz_id, q.quiz_name, SUM(sq.student_score) 
    AS totalScore, SUM(qs.points) AS possiblePoints, sq.date_taken AS date_taken, sq.users_id
    FROM scores_per_question sq
    JOIN quizzes q ON q.id = sq.quiz_id
    JOIN questions qs ON qs.id = sq.question_id
    WHERE sq.users_id = ${user_id}
    GROUP BY sq.quiz_id 
    ORDER BY sq.date_taken desc
  `;

  return await pool.query(sql);
};

const getLastFiveFromHistory = async (user_id) => {
  const sql = `
    SELECT distinct sq.quiz_id, q.quiz_name, SUM(sq.student_score) 
    AS totalScore, SUM(qs.points) AS possiblePoints, sq.date_taken AS date_taken
    FROM scores_per_question sq
    JOIN quizzes q ON q.id = sq.quiz_id
    JOIN questions qs ON qs.id = sq.question_id
    WHERE sq.users_id = ${user_id}
    GROUP BY sq.quiz_id
    ORDER BY sq.date_taken desc
    LIMIT 5;
`;

  return await pool.query(sql);
};

const getHistoryByCategory= async (user_id, category_id) => {
  const sql = `
    SELECT distinct sq.category_id, sq.quiz_id, q.quiz_name, SUM(sq.student_score) 
    AS totalScore, SUM(qs.points) AS possiblePoints, sq.date_taken AS date_taken
    FROM scores_per_question sq
    JOIN quizzes q ON q.id = sq.quiz_id
    JOIN questions qs ON qs.id = sq.question_id
    WHERE sq.users_id = ${user_id} AND sq.category_id = ${category_id}  GROUP BY sq.quiz_id
    ORDER BY sq.date_taken desc;
`;

  return await pool.query(sql);
};
const getStudentScoresByQuizId = async (quiz_id) => {
  const sql = `
    SELECT u.username, u.firstname, u.lastname, u.avatar, qz.quiz_name, SUM(spq.student_score) as score, spq.date_taken
    FROM scores_per_question spq
    JOIN quizzes qz ON qz.id = spq.quiz_id 
    JOIN users u ON u.id = spq.users_id
    WHERE spq.quiz_id = ${quiz_id}
    GROUP BY spq.users_id
    ORDER BY spq.date_taken desc;
  `;

  const result = await pool.query(sql);
  return result;
};

const getQuizWithCorrectAnswers = async (category_id, quiz_id) => {
  const sql = `
    SELECT a.quiz_id as quiz_id, a.question_id, q.points, GROUP_CONCAT(a.id) AS answers_id
    FROM answers a
    JOIN questions q
    ON q.id = a.question_id
    WHERE a.is_correct = 1 and a.quiz_id= ${quiz_id} and a.category_id = ${category_id}
    GROUP BY a.question_id;
  `;
  return await pool.query(sql);
};

const submitUserAnswers = async (results) => {
  const answersToInsert = results.reduce((acc, r) => {
    acc += `
    ('${r.student_score}', ${r.date_taken}, ${r.question_id}, ${r.quiz_id}, ${r.category_id}, ${r.users_id}),`;
    return acc;
  }, ``);

  const sql = `
    INSERT INTO scores_per_question (student_score, date_taken, question_id, quiz_id, category_id, users_id) 
    VALUES${answersToInsert.slice(0, -1)};
    `;
  const result = await pool.query(sql);
};


const searchQuiz = async (value) => {
  const sql = `
    SELECT quiz_name FROM quizzes
    WHERE quiz_name LIKE '%${value}%';
    `;
  return await pool.query(sql);
};

export default {
  createNewQuiz,
  createQuestionsAndAnswers,
  getQuestionsByQuizID,
  getAnswersByQuizID,
  getQuizDetailsById,
  getByCategoryId,
  getByCategoryIdStudent,
  getQuizByQuizId,
  getQuizWithCorrectAnswers,
  submitAnswers,
  checkIfUserDoneQuiz,
  getScoresFromQuizByUserIdAndQuizId,
  getHistory,
  getLastFiveFromHistory,
  getHistoryByCategory,
  getStudentScoresByQuizId,
  getLast5QuizzesByTeacher,
  submitUserAnswers,
  searchQuiz
};
