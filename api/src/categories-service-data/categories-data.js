import pool from '../pool.js';

const getAllCategories = async () => {
  const sql = `
    SELECT id as category_id, category_name, category_photo FROM categories;
    `;
  return await pool.query(sql);
};

const searchCategory = async (value) => {
  const sql = `
    SELECT id as category_id, category_name, category_photo FROM categories
    WHERE category_name LIKE '%${value}%';
    `;
  return await pool.query(sql);
};

const createNewCategory = async (category) => {
  const insertCategoryIfNotExists = `
    INSERT INTO categories(category_name)
    SELECT * FROM (SELECT '${category}') AS tmp
    WHERE NOT EXISTS (
        SELECT category_name FROM categories WHERE category_name = '${category}'
    ) LIMIT 1;
`;

  const newCategory = await pool.query(insertCategoryIfNotExists, [category]);
  return newCategory[0];
};

const getBy = async (column, id) => {
  const sql = `
    SELECT * FROM categories
    WHERE ${column} = '${id}';
    `;
  const result = await pool.query(sql);


  return result;
};

const updatePhoto = async (id, path) => {
  const sql = `
      UPDATE categories SET
      category_photo = ?
      WHERE id = ?
  `;

  return await pool.query(sql, [path, id]);
}


export default {
  getAllCategories,
  searchCategory,
  createNewCategory,
  getBy,
  updatePhoto
};
