
const getAllCategories = (categoriesData) => {
  return async (filter) => {
    return filter
      ? await categoriesData.searchCategory(filter)
      : await categoriesData.getAllCategories();
  };
};

const getCategoryById = (categoriesData) => {
  return async (id) => {
    return await categoriesData.getBy('id', id);
  };
};

const createCategory = (categoriesData) => {
  return async (category) => {
    const { category_name } = category;

    const existingCategory = await categoriesData.searchCategory(category_name);

    if (existingCategory[0]) {
      return {
        error: 'Category Exists',
        newCategory: null,
      };
    } else {
      const newCategory = await categoriesData.createNewCategory(category_name);

      return {
        error: null,
        newCategory: await categoriesData.searchCategory(category_name),
      };
    }
  };
};

const updateCategoryPhoto = usersData => {
  return async (id, path) => {
    const result = await usersData.updatePhoto(id, path);
    return { error: result.affectedRows > 0 ? null : serviceErrors.UNSPECIFIED_ERROR };
  }
};

export default {
  getAllCategories,
  createCategory,
  getCategoryById,
  updateCategoryPhoto
};
