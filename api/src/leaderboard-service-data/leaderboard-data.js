import pool from '../pool.js';

const getLeaderboardScores = async () => {
  const sql = `
      SELECT u.id as user_id, u.username, sum(sq.student_score) as total_score, u.avatar, u.firstname, u.lastname
      FROM scores_per_question sq
      JOIN users u
      ON u.id = sq.users_id
      WHERE u.role = "student"
      GROUP by u.username
      ORDER by total_score desc;
    `;
  return await pool.query(sql);
};

const getLeaderboardScoresTopTen = async () => {
  const sql = `
      SELECT u.id as user_id, u.username, sum(sq.student_score) AS total_score, u.avatar, u.firstname, u.lastname
      FROM scores_per_question sq
      JOIN users u
      ON u.id = sq.users_id
      WHERE u.role = "student"
      GROUP by u.username
      ORDER by total_score desc
      LIMIT 10;
    `;
  return await pool.query(sql);
};

const searchLeaderBoardScores = async (search) => {
  const sql = `
      SELECT u.id AS user_id, u.username, sum(sq.student_score) AS total_score, u.avatar, u.firstname, u.lastname
      FROM scores_per_question sq
      JOIN users u
      ON u.id = sq.users_id
      WHERE u.role = "student" AND u.username LIKE '%${search}%'
      GROUP BY u.username
      ORDER BY total_score desc;
    `;

  return await pool.query(sql);
}

export default {
  getLeaderboardScores,
  getLeaderboardScoresTopTen,
  searchLeaderBoardScores
}
