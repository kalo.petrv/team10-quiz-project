import ServiceErrors from '../service-errors/service-errors.js';

const getStudentLeaderboard = (leaderBoardData) => {
    return async (filter) => {
      if(filter){
        const leaderboard = await leaderBoardData.searchLeaderBoardScores(filter);
        if (!leaderboard || !leaderboard[0]) {
          return {
            error: ServiceErrors.RECORD_NOT_FOUND,
            leaderboard: null,
          };
        } else {
          return {
            error: null,
            leaderboard,
          };
        }
      }
      const leaderboard = await leaderBoardData.getLeaderboardScores();
      
      if (!leaderboard || !leaderboard[0]) {
        return {
          error: ServiceErrors.RECORD_NOT_FOUND,
          leaderboard: null,
        };
      } else {
        return {
          error: null,
          leaderboard,
        };
      }
    };
  };

  const getStudentLeaderboardTopTen = (leaderBoardData) => {
    return async () => {
      const leaderboard = await leaderBoardData.getLeaderboardScoresTopTen();
  
      if (!leaderboard) {
        return {
          error: ServiceErrors.RECORD_NOT_FOUND,
          leaderboard: null,
        };
      } else {
        return {
          error: null,
          leaderboard,
        };
      }
    };
  };

  export default {
    getStudentLeaderboard,
    getStudentLeaderboardTopTen
  }