export const DB_CONFIG = {
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '1234',
    database: 'quizdb1'
};

export const PORT = 4000;

export const PRIVATE_KEY = 'sekreten_chasten_klu4';

// 60 mins * 60 secs
export const TOKEN_LIFETIME = 60 * 60;

export const DEFAULT_USER_ROLE = 'User';