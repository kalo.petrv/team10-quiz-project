import express from 'express';
import categoriesService from '../categories-service-data/categories-service.js';
import categoriesData from '../categories-service-data/categories-data.js';
import quizzesService from '../quizzes-service-data/quizzes-service.js';
import quizzesData from '../quizzes-service-data/quizzes-data.js';
import usersData from '../user-service-data/user-data.js';
import usersService from '../user-service-data/user-service.js';
import multer from 'multer';
import storage from './../storage.js';
import ServiceErrors from './../service-errors/service-errors.js';
import leaderboardService from '../leaderboard-service-data/leaderboard-service.js';
import leaderboardData from '../leaderboard-service-data/leaderboard-data.js';
import serviceErrors from '../service-errors/service-errors.js';
import {
  authMiddleware,
  roleMiddleware,
  tokenMiddleware,
} from '../auth/auth-middleware.js';


const studentController = express.Router();

studentController

  .get(
    '/categories',
    authMiddleware,
    roleMiddleware('student'),
    tokenMiddleware(),
    async (req, res) => {
      const { search } = req.query;
      const categories = await categoriesService.getAllCategories(
        categoriesData
      )(search);

      res.status(200).send(categories);
    }
  )

  .get(
    '/quizzes',
    authMiddleware,
    roleMiddleware('student'),
    tokenMiddleware(),
    async (req, res) => {
      const { search } = req.query;
      const quizzes = await quizzesService.getAllQuizzes(quizzesData)(search);

      res.status(200).send(quizzes);
    }
  )

  .get(
    '/categories/:id/history',
    authMiddleware,
    roleMiddleware('student'),
    tokenMiddleware(),
    async (req, res) => {
      const id = req.params.id;
      const user_id = req.user.id;
      const {
        error,
        history,
      } = await quizzesService.getStudentHistoryByCategory(quizzesData)(
        user_id,
        id
      );

      if (error) {
        res
          .status(409)
          .send({ error: 'No sybmitted quizzes in this category' });
      } else {
        res.status(200).send(history);
      }
    }
  )

  .get(
    '/categories/:id/quizzes',
    authMiddleware,
    roleMiddleware('student'),
    tokenMiddleware(),
    async (req, res) => {
      const id = req.params.id;
      const user_id = req.user.id;
      const {
        error,
        quizzes,
      } = await quizzesService.getQuizzesByCategoryIDStudent(quizzesData)(
        id,
        user_id
      );
      if (error) {
        res.status(409).send({ error: 'No quizzes in this category' });
      } else {
        res.status(200).send(quizzes);
      }
    }
  )

  .get(
    '/categories/:id/quizzes/:quizId',
    authMiddleware,
    roleMiddleware('student'),
    tokenMiddleware(),
    async (req, res) => {
      const { id, quizId } = req.params;
      const user_id = req.user.id;

      const { error, quiz } = await quizzesService.getQuizByID(quizzesData)(
        +quizId,
        +user_id,
        +id
      );

      if (+error === 5) {
        res.status(404).send({ error: 'You have already active quiz!' });
      }
      if (+error === 6) {
        res.status(404).send({ error: 'The quiz is already done!' });
      }
      if (error) {
        res.status(404).send({ error: 'Not a valid quiz' });
      } else {
        res.status(200).send(quiz);
      }
    }
  )

  .post(
    '/categories/:categoryId/quizzes/:quizId/solutions',
    authMiddleware,
    roleMiddleware('student'),
    tokenMiddleware(),
    async (req, res) => {
      const { categoryId, quizId } = req.params;
      const answers = req.body;
      const user_id = req.user.id;
      const { error, score, possiblePoints } = await quizzesService.submitQuiz(
        quizzesData
      )(+categoryId, quizId, answers, user_id);

      if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
        return res.status(400).send({
          error: 'You can do the quiz only one time/the quiz is not valid',
        });
      }
      if (error === ServiceErrors.RECORD_NOT_FOUND) {
        return res.status(400).send({ error: 'The quiz is not valid' });
      } else {
        return res.status(200).send({ score, possiblePoints });
      }
    }
  )

  .get(
    '/categories/:categoryId/quizzes/:quizId/score',
    authMiddleware,
    roleMiddleware('student'),
    tokenMiddleware(),
    async (req, res) => {
      const { categoryId, quizId } = req.params;
      const user_id = req.user.id;

      const { error, scores } = await quizzesService.getScore(quizzesData)(
        +categoryId,
        +user_id,
        +quizId
      );
      if (error) {
        res.status(409).send({ error: 'The quiz is not done' });
      } else {
        res.status(200).send({ scores: scores });
      }
    }
  )

  .get(
    '/history',
    authMiddleware,
    roleMiddleware('student'),
    tokenMiddleware(),
    async (req, res) => {
      const user_id = req.user.id;
      const { error, history } = await quizzesService.getStudentHistory(
        quizzesData
      )(user_id);

      if (error) {
        res.status(409).send({ error: 'There are no quizzes to show.' });
      } else {
        res.status(200).send(history);
      }
    }
  )

  .get(
    '/history/last',
    authMiddleware,
    roleMiddleware('student'),
    tokenMiddleware(),
    async (req, res) => {
      const user_id = req.user.id;
      const { error, history } = await quizzesService.getStudentHistoryLastFive(
        quizzesData
      )(user_id);

      if (error) {
        res.status(409).send({ error: 'There are no quizzes to show.' });
      } else {
        res.status(200).send(history);
      }
    }
  )
  .get(
    '/leaderboard',
    authMiddleware,
    roleMiddleware('student'),
    tokenMiddleware(),
    async (req, res) => {
      const { search } = req.query;
      const {
        error,
        leaderboard,
      } = await leaderboardService.getStudentLeaderboard(leaderboardData)(search);

      if (error) {
        res.status(409).send({ error: 'There are no students to show.' });
      } else {
        res.status(200).send(leaderboard);
      }
    }
  )

  .get(
    '/leaderboard/top',
    authMiddleware,
    roleMiddleware('student'),
    tokenMiddleware(),
    async (req, res) => {
      const {
        error,
        leaderboard,
      } = await leaderboardService.getStudentLeaderboardTopTen(leaderboardData)();

      if (error) {
        res.status(409).send({ error: 'There are no students to show.' });
      } else {
        res.status(200).send(leaderboard);
      }
    }
  )

  .post(
    '/avatar',
    authMiddleware,
    multer({ storage: storage }).single('avatar'),
    async (req, res) => {
      const user_id = req.user.id;
      const { error } = await usersService.updateAvatar(usersData)(
        user_id,
        req.file.filename
      );

      if (error) {
        res.sendStatus(500);
      } else {
        res.status(200).send({
          path: req.file.filename,
        });
      }
    }
  )

  .get(
    '/activequiz',
    authMiddleware,
    roleMiddleware('student'),
    tokenMiddleware(),
    async (req, res) => {
      const user_id = req.user.id;
      const { result } = await usersService.getUsersActiveQuiz(usersData)(user_id);

      return res.status(200).send(result);
    }
  );


export default studentController;
