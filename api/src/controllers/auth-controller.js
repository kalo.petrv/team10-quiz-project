import express from 'express';
import createToken from '../auth/create-token.js';
import usersService from '../user-service-data/user-service.js';
import usersData from '../user-service-data/user-data.js';
import {
  createValidator,
  createUserSchema,
  signInUserSchema,
} from '../validations/index.js';
import {
  authMiddleware,
  tokenMiddleware,
} from '../auth/auth-middleware.js';

const authController = express.Router();

authController
  .post(
    '/register/only-for-teachers',
    createValidator(createUserSchema),
    async (req, res) => {
      const createData = req.body;
      createData.role = 'teacher'
      const { error, user } = await usersService.signUp(usersData)(
        createData,
      );
      if (error) {
        res.status(409).send({ error: 'Username not available' });
      } else {
        res.status(201).send(user);
      }
    }
  )
  .post('/signup', createValidator(createUserSchema), async (req, res) => {
    const createData = req.body;
    const { error, user } = await usersService.signUp(usersData)(
      createData
    );
    if (error) {
      res.status(409).send({ error: 'Username not available' });
    } else {
      res.status(201).send(user);
    }
  })

  .post('/signin', createValidator(signInUserSchema), async (req, res) => {
    const { username, password } = req.body;
    const { error, user } = await usersService.signIn(usersData)(
      username,
      password
    );

    if (error) {
      return res.status(404).send({ error: 'Invalid username/password' });
    }

    const payload = {
      sub: user.id,
      username: user.username,
      role: user.role,
      firstName: user.firstName,
      lastName: user.lastName,
      avatar: user.avatar,
      activeQuiz: user.active_quiz,
    };

    const token = createToken(payload);

    return res.status(200).send({ token: token });
  })

  .delete('/signout', authMiddleware, tokenMiddleware(), async (req, res) => {
    const token = req.headers.authorization;

    const newToken = await usersService.signOut(usersData)(token);

    res.status(200).send({ message: `Sign out success` });
  })

  .get('/info', authMiddleware, tokenMiddleware(), async (req, res) => {
    const user_id = req.user.id;
    const { result } = await usersService.getUsersInfo(usersData)(user_id);

    return res.status(200).send(result);
  });

export default authController;
