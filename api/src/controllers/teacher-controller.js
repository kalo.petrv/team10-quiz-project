import express from 'express';
import categoriesService from '../categories-service-data/categories-service.js';
import categoriesData from '../categories-service-data/categories-data.js';
import quizzesService from '../quizzes-service-data/quizzes-service.js';
import quizzesData from '../quizzes-service-data/quizzes-data.js';
import {
  authMiddleware,
  roleMiddleware,
  tokenMiddleware,
} from '../auth/auth-middleware.js';
import leaderboardService from '../leaderboard-service-data/leaderboard-service.js';
import leaderboardData from '../leaderboard-service-data/leaderboard-data.js';
import multer from 'multer';
import storage from './../storage.js';
import ServiceErrors from './../service-errors/service-errors.js';
import { createValidator } from '../validations/validator-middleware.js';
import { createCategorySchema } from '../validations/schemas/create-category.js';
import { createQuizSchema } from '../validations/schemas/create-quiz.js';

const teacherController = express.Router();

teacherController
  .get('/categories', async (req, res) => {
    const { search } = req.query;
    const categories = await categoriesService.getAllCategories(categoriesData)(
      search
    );
    res.status(200).send(categories);
  })
  .get('/categories/:id', async (req, res) => {
    const { id } = req.params;
    const category = await categoriesService.getCategoryById(categoriesData)(
      id
    );
    res.status(200).send(category);
  })
  .post(
    '/categories',
    createValidator(createCategorySchema),
    async (req, res) => {
      const category = req.body;

      const { error, newCategory } = await categoriesService.createCategory(
        categoriesData
      )(category);
      if (error) {
        res.status(409).send({ error: 'Category Already exists' });
      } else {
        res.status(201).send({ message: 'New Category Created!', newCategory });
      }
    }
  )
  .get('/:id/quizzes/', async (req, res) => {
    const user_id = req.params.id || 1;

    const quizzes = await quizzesService.getQuizzesByTeacher(quizzesData)(
      user_id
    );
    res.status(200).send(quizzes);
  })
  .post('/quizzes', createValidator(createQuizSchema), async (req, res) => {
    const quiz = req.body.quiz.quiz_details;
    const user_id = req.body.user_id;

    const { error, newQuiz } = await quizzesService.createQuiz(quizzesData)(
      quiz,
      user_id
    );
    if (error) {
      res.status(400).send({ error: error });
    } else {
      res.status(201).send({ message: 'New Quiz created:', newQuiz: newQuiz });
    }
  })
  .get('/categories/:id/quizzes', async (req, res) => {
    const id = req.params.id;

    const { error, quizzes } = await quizzesService.getQuizzesByCategoryID(
      quizzesData
    )(id);
    if (error) {
      res.status(409).send({ error: 'No quizzes in this category' });
    } else {
      res.status(200).send(quizzes);
    }
  })
  .get('/categories/:id/quizzes/:quizId', async (req, res) => {
    const { id, quizId } = req.params;
    const user_id = req.user.id;

    const { error, quiz } = await quizzesService.getQuizByID(quizzesData)(
      +quizId,
      +user_id,
      +id
    );

    if (error) {
      res.status(404).send({ error: 'Not a valid quiz' });
    } else {
      res.status(200).send(quiz);
    }
  })
  .post(
    '/categories/:categoryId/quizzes/:quizId/solutions',
    async (req, res) => {
      const { categoryId, quizId } = req.params;
      const answers = req.body;
      const user_id = req.user.id;
      const { error, score, possiblePoints } = await quizzesService.submitQuiz(
        quizzesData
      )(+categoryId, quizId, answers, user_id);

      if (error === ServiceErrors.OPERATION_NOT_PERMITTED) {
        return res.status(400).send({
          error: 'You can do the quiz only one time/the quiz is not valid',
        });
      }
      if (error === ServiceErrors.RECORD_NOT_FOUND) {
        return res.status(400).send({ error: 'The quiz is not valid' });
      }

      return res.status(200).send({ score, possiblePoints });
    }
  )
  .get('/categories/:id/quizzes/:id/scores', async (req, res) => {
    const quiz_id = req.params.id;

    const { error, scores } = await quizzesService.getAllStudentsScoresForQuiz(
      quizzesData
    )(+quiz_id);
    if (error) {
      res.status(409).send({ error: 'The quiz is not completed by anyone' });
    } else {
      res.status(200).send({ scores: scores });
    }
  })

  .post(
    '/:id/photo',
    multer({ storage: storage }).single('photo'),
    async (req, res) => {
      const { id } = req.params;
      const { error } = await categoriesService.updateCategoryPhoto(
        categoriesData
      )(id, req.file.filename);

      if (error) {
        res.sendStatus(500);
      } else {
        res.status(200).send({
          path: req.file.filename,
        });
      }
    }
  );

export default teacherController;
