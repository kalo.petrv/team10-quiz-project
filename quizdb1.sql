-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema quizdb1
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema quizdb1
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `quizdb1` DEFAULT CHARACTER SET utf8 ;
USE `quizdb1` ;

-- -----------------------------------------------------
-- Table `quizdb1`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdb1`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `category_name` VARCHAR(45) NOT NULL,
  `category_photo` VARCHAR(265) NULL DEFAULT '1606061988531_298456413.jpg',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 28
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quizdb1`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdb1`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `password` VARCHAR(250) NOT NULL,
  `role` VARCHAR(45) NOT NULL DEFAULT 'student',
  `has_active_quiz` TINYINT(4) NULL DEFAULT 0,
  `avatar` VARCHAR(250) NULL DEFAULT '1605578525021_344234547.jpg',
  `active_quiz` INT(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quizdb1`.`quizzes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdb1`.`quizzes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `quiz_name` VARCHAR(45) NOT NULL DEFAULT 'quizname',
  `category_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `category_id`, `users_id`),
  INDEX `fk_quizzes_categories_idx` (`category_id` ASC) VISIBLE,
  INDEX `fk_quizzes_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_quizzes_categories`
    FOREIGN KEY (`category_id`)
    REFERENCES `quizdb1`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_quizzes_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `quizdb1`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 48
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quizdb1`.`questions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdb1`.`questions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `question` VARCHAR(250) NOT NULL DEFAULT 'question',
  `points` INT(11) NOT NULL DEFAULT 0,
  `quiz_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  `q_type` TINYINT(4) NULL DEFAULT 0,
  PRIMARY KEY (`id`, `quiz_id`, `category_id`),
  INDEX `fk_questions_quizzes1_idx` (`quiz_id` ASC, `category_id` ASC) VISIBLE,
  CONSTRAINT `fk_questions_quizzes1`
    FOREIGN KEY (`quiz_id` , `category_id`)
    REFERENCES `quizdb1`.`quizzes` (`id` , `category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 68
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quizdb1`.`answers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdb1`.`answers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `answer` VARCHAR(250) NOT NULL DEFAULT 'null',
  `is_correct` TINYINT(4) NOT NULL DEFAULT 0,
  `question_id` INT(11) NOT NULL,
  `quiz_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `question_id`, `quiz_id`, `category_id`),
  INDEX `fk_answers_questions_idx` (`question_id` ASC, `quiz_id` ASC, `category_id` ASC) VISIBLE,
  CONSTRAINT `fk_answers_questions`
    FOREIGN KEY (`question_id` , `quiz_id` , `category_id`)
    REFERENCES `quizdb1`.`questions` (`id` , `quiz_id` , `category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 204
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quizdb1`.`scores_per_question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdb1`.`scores_per_question` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `student_score` INT(11) NOT NULL DEFAULT 0,
  `date_taken` BIGINT(20) NULL DEFAULT NULL,
  `question_id` INT(11) NOT NULL,
  `quiz_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `question_id`, `quiz_id`, `category_id`, `users_id`),
  INDEX `fk_scores_questions_questions1_idx` (`question_id` ASC) VISIBLE,
  INDEX `fk_scores_questions_quizzes1_idx` (`quiz_id` ASC, `category_id` ASC) VISIBLE,
  INDEX `fk_scores_per_question_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_scores_per_question_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `quizdb1`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_scores_questions_questions1`
    FOREIGN KEY (`question_id`)
    REFERENCES `quizdb1`.`questions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_scores_questions_quizzes1`
    FOREIGN KEY (`quiz_id` , `category_id`)
    REFERENCES `quizdb1`.`quizzes` (`id` , `category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 82
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quizdb1`.`token_blacklist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdb1`.`token_blacklist` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `token` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO `users` VALUES (1,'teacher1','hans','bard','1234','teacher',0,'1605578525021_344234547.jpg',0),(4,'pesho2','Petar','Petrov','$2b$10$SS3W.V/pNt1cNQzQdzHNEeOzbPYhCmIpIUmHYBjBueYFHaiB9cr3i','student',0,'1606559495313_408065696.jpg',0),(5,'uchitel','Georgi','Georgiev','$2b$10$yEa/jAT4wFPJ06ws92tqu.v62IknIMMgkgYDV8uY8INbelQ/yEjSe','teacher',0,'1606909384526_374359399.png',48),(6,'uchitel2','Maria','Georgieva','$2b$10$XJgX4oXl8ztm.jlK08VO2ODLsCHpSYuPLR83BtSEHFZCv5qjlvRgm','teacher',0,'1605578525021_344234547.jpg',0),(7,'uchitel3','Ivan','Petrov','$2b$10$ZPyojaLO20gvP/WMXewlFe6NJ5Rw8DZo4odf6nYqq1rl.NxUuAjBi','student',0,'1605578525021_344234547.jpg',0),(8,'maria','Maria','Ivanonva','$2b$10$uw8.HapiQe7XJ00I5juxR.25Eqway9tCqqmnFcqZquE0AmNAWxFVW','student',0,'1606911227196_504150377.png',0),(9,'ivan4o','Ivan','Ivanov','$2b$10$nDY.yqnMlPPCz2BCIXInw.dyjtRQgnVM955j9ZrZVwEw/9/q.a4gG','student',0,'1606559604310_099550478.png',0),(10,'anastasia34','Anastasia','Ivanova','$2b$10$fFih4gtfkKeoizWchya70OGrS8B9EMWO0AlJeXvu6p8ib.8ogIviO','student',0,'1606912082880_643802938.png',0),(11,'georgi','Georgi','Ivanov','$2b$10$XjsvgN222PAZEqvkxwHSd.z6kpjrW3fBr9Y8SVCd7rQ0ggOAxf1pe','student',0,'1606917557121_841974384.png',0),(12,'uchitel4','Georgi','Petrov','$2b$10$LtxppVN8uzN5Z6Ks0hdsRuqFQDBVNMbItAF/fqRIr1LClKLNL0rVe','student',0,'1605578525021_344234547.jpg',0),(13,'uchitel5','Maria','Ivanova','$2b$10$2pkUYPSob8iVo5Ql/H2J2O0B3hylxCbesu8tKAihwhaFApZz.jhAu','student',0,'1605578525021_344234547.jpg',0),(14,'Maria-Uchitel','Maria','Dimitrova','$2b$10$txm0UdsuzAbc1hk76GsWZ.ZmHoIK3dDSjn2rXkuC5Ayj0xQ6hZkB2','student',0,'1605578525021_344234547.jpg',0),(15,'uchitel6','Ivan','Dimitrov','$2b$10$wDB6VFIyVWWTLZql0XsvPeg0xQqG/WQQxQECGqT6PADWKp/.jXwtG','teacher',0,'1605578525021_344234547.jpg',46),(16,'anna24','Anna','Georgieva','$2b$10$3Vrvt4OALKlUX2VY9QmeEeOthDSr8hbZfjOtKB/IVn6/Puf65B6xe','student',0,'1606927221473_747996151.png',0),(17,'kiril16','Kiril','Stefanov','$2b$10$rORQ3FFFtRskUyho7tdU7uzkpSMVgXuocdo8t6poV.6b8VAcBUNsi','student',0,'1606927849497_171577887.png',0),(18,'niki28','Nikolay','Todorov','$2b$10$HdoZqOe7nTgNfb19h8NkcOnx9Mh7a/c01JAwEc5yYrDSuFxxs8tDq','student',0,'1606928495126_733621136.png',0),(19,'geri13','Gergana','Ilieva','$2b$10$SipGt4p5gDQ1XXs7GFSw6eK5F3yha4fUTBeBT/vdSSbtEoxKNiFQm','student',0,'1606928725409_292595600.png',0),(20,'lili5','Lili','Ivanova','$2b$10$I.k6TDQUTL/y9wY3RJ/wIOeiIsj9WCWgtPVa5WwV8mBrSNn/mX1wO','student',0,'1606928875726_792279481.png',0),(22,'joro28','Georgi','Dimitrov','$2b$10$iimuX4y8MEljIc/inH5oee8wI6ng54j3wnQl2gohNs9IsHw3ktMKa','student',0,'1606929073846_182948540.png',0),(23,'qna21','Yana','Kirilova','$2b$10$uD1F.dBaJS4Sjjh0WLP3QedRpfF5NVCqrmnRINiuYYSJL1gdRbB4m','student',0,'1606930909823_884380002.png',0),(24,'ivanski','Ivan','Mitov','$2b$10$21Z4nU9J9O0ftpyX6mjGe.Nc4VC0TZgWaDaTIgpOB3vnPNnNUO9g.','student',0,'1606931938765_284808366.png',0),(25,'misho18','Mihail','Mihaylov','$2b$10$ZT.g0GnU8nM/CkgGFX9MqeP4zjA4hNdsvlFMMQdfju6cKhKp4jQbm','student',0,'1606932545058_433047407.png',0),(26,'henry2','Henry','Atanasov','$2b$10$Y83Kp5Hv68SvAHoqj0aDk.LtVBzhVH2UA2viW1L/Glk4xjTupmqVC','student',0,'1606933928148_508910361.png',0),(27,'dani15','Daniel','Georgiev','$2b$10$EYpseybqOxo7JWIH9rAtKeUbJFv.sMKZSwjdf5b523VpVcpeMNl.6','student',0,'1605578525021_344234547.jpg',0),(28,'jorje13','Jorj','Ilchev','$2b$10$Sw2LObqeuWFMjts.xqctteTbpIB7YLaX8TGU/UkIP5y/8tYzJQOUO','student',0,'1606933763702_374974012.png',0);
INSERT INTO `token_blacklist` VALUES (1,'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjQsInVzZXJuYW1lIjoicGVzaG8yIiwicm9sZSI6InN0dWRlbnQiLCJpYXQiOjE2MDQ5OTc2ODcsImV4cCI6MTYwNTAwMTI4N30.veCatwMfNpYcLvsXtvCUrev3PqACKJy9HFY8MZwjf4U'),(2,'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoidWNoaXRlbCIsInJvbGUiOiJ0ZWFjaGVyIiwiaWF0IjoxNjA0OTk4OTUyLCJleHAiOjE2MDUwMDI1NTJ9.gVv6N_OvVJ-D4zIkE7a0XlTSkxjc3fOsXXOwU8uKmUQ'),(3,'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOjUsInVzZXJuYW1lIjoidWNoaXRlbCIsInJvbGUiOiJ0ZWFjaGVyIiwiaWF0IjoxNjA1MDA5MTgxLCJleHAiOjE2MDUwMTI3ODF9.eWLagBq7WeZYmxYtg9KAOcZI3Sh1myBAIKxzd0O5X4U');
INSERT INTO `categories` VALUES (1,'Animals','1606917731654_414557853.png'),(2,'Trees','1606905573771_895709005.png'),(4,'Math','1606909301381_646390643.png'),(5,'Literature','1606907961149_542612593.png'),(25,'Physics','1606907970921_822465941.png'),(26,'Biology','1606907985740_038989753.png'),(27,'Games','1606907995750_805253337.png');
INSERT INTO `quizzes` VALUES (7,'Animal colors',1,1),(8,'Animal sounds',1,1),(20,'Tree Parts',2,5),(24,'Basic Addition',4,5),(25,'simple add',4,5),(30,'Animal Science',1,5),(31,'Bio 101',26,5),(32,'Tree Types',2,5),(33,'Basic Multiplication',4,5),(34,'Basic Subtraction',4,5),(44,'Lord Of The Rings',5,15),(45,'Harry Potter',5,15),(46,'Physics 101',25,15),(47,'Board Games',27,5),(48,'Physics and Forces ',25,5);
INSERT INTO `questions` VALUES (3,'What color is a jiraffe?',1,7,1,0),(4,'What color is a dolphin?',1,7,1,0),(5,'What animal barks (woof, woof)?',1,8,1,0),(6,'What animal meows (meow, meow)?',1,8,1,0),(19,'Which of those are parts of a tree?',1,20,2,1),(20,'Which of those is mostly extracted from trees?',1,20,2,0),(21,'Which of those is not a common tree color',1,20,2,0),(22,'5+2=?',1,24,4,0),(23,'6+7=?',1,24,4,0),(24,'10+13=?',1,24,4,1),(25,'20+60=?',1,24,4,0),(26,'Bonus: 5*5=?',2,24,4,0),(37,'Which of these helps an animal protect itself from predators?',1,30,1,0),(38,'_____ is when an animal becomes inactive in the winter.',1,30,1,0),(39,'The scientific term for where an organism lives.',1,30,1,0),(40,'What does your cardiovascular system do?',2,31,26,0),(41,'Two bones join together at the ____.',2,31,26,0),(42,'What equipment is best for viewing cells?',2,31,26,0),(43,'What trees are found on beaches?',1,32,2,0),(44,'What Trees are found in North America?',2,32,2,1),(45,'What Type of Tree is the Christmas Tree?',1,32,2,0),(46,'5*5 =?',1,33,4,0),(47,'7*7 = ?',1,33,4,0),(48,'2*3*2 = ? ',2,33,4,0),(49,'10-5 = ?',1,34,4,0),(50,'26-9 = ? ',1,34,4,1),(51,'40-23-4=?',2,34,4,0),(54,'Who was the main evil character that forged the ring?',1,44,5,0),(55,'Hobbits lived in ...?',1,44,5,0),(56,'Who is the main character in the Lord of the rings?',1,44,5,0),(57,'Who is the author of the Lord of the Rings?',1,44,5,0),(58,'Which of these is not one of Harry\'s friends?',1,45,5,0),(59,'Is Harry Potter related to Ron?',1,45,5,0),(60,'What house did the sorting hat want to place Harry in?',1,45,5,0),(61,'Who are Harry\'s best friends?',1,45,5,0),(62,'Molecules can have atoms from more than one chemical element.',2,46,25,0),(63,'Atoms are most stable when their outer shells are full.',2,46,25,0),(64,'Electrons are larger than molecules.',2,46,25,0),(65,'Which of these is not a board game?',1,47,27,0),(66,'The main goal in Chess is to?',1,47,27,0),(67,'Select the games that include dice',2,47,27,1),(68,'What is a force that occurs when one object rubs against another object?',2,48,25,0),(69,'What is the force that pulls any two objects toward each other?',2,48,25,0);
INSERT INTO `answers` VALUES (1,'red',0,4,7,1),(2,'yellow',0,4,7,1),(3,'green',0,4,7,1),(4,'green',0,3,7,1),(5,'blue',0,3,7,1),(6,'yellow',1,3,7,1),(7,'blue',1,4,7,1),(8,'red',0,3,7,1),(9,'cat',1,6,8,1),(10,'fox',0,6,8,1),(11,'wolf',0,6,8,1),(12,'cat',0,5,8,1),(13,'dog',1,5,8,1),(14,'sheep',0,5,8,1),(15,'mouse',0,5,8,1),(16,'dog',0,6,8,1),(41,'leg',0,19,20,2),(42,'trunk',1,19,20,2),(43,'leaves',1,19,20,2),(44,'hands',0,19,20,2),(45,'wood',1,20,20,2),(46,'honey',0,20,20,2),(47,'leaves',0,20,20,2),(48,'roots',0,20,20,2),(49,'green',0,21,20,2),(50,'brown',0,21,20,2),(51,'grey',0,21,20,2),(52,'purple',1,21,20,2),(53,'6',0,22,24,4),(54,'8',0,22,24,4),(55,'7',1,22,24,4),(56,'9',0,22,24,4),(57,'13',1,23,24,4),(58,'12',0,23,24,4),(59,'15',0,23,24,4),(60,'14',0,23,24,4),(61,'23',1,24,24,4),(62,'13',0,24,24,4),(63,'twenty three',1,24,24,4),(64,'thirteen',0,24,24,4),(65,'70',0,25,24,4),(66,'80',1,25,24,4),(67,'90',0,25,24,4),(68,'95',0,25,24,4),(69,'30',0,26,24,4),(70,'20',0,26,24,4),(71,'25',1,26,24,4),(72,'35',0,26,24,4),(94,'Food',0,37,30,1),(95,'Camouflage',1,37,30,1),(96,'Fire',0,37,30,1),(97,'Migration',0,37,30,1),(98,'Consumption',0,38,30,1),(99,'Eating',0,38,30,1),(100,'Extinction',0,38,30,1),(101,'Hibernation',1,38,30,1),(102,'Habitat',1,39,30,1),(103,'Ocean',0,39,30,1),(104,'Tree',0,39,30,1),(105,'Grass',0,39,30,1),(106,' Delivers calcium to your bones',0,40,31,26),(107,'Digests food',0,40,31,26),(108,'Circulates blood and oxygen around your body',1,40,31,26),(109,'Controls your limbs',0,40,31,26),(110,'Ribs',0,41,31,26),(111,'Spine',0,41,31,26),(112,'Skeleton',0,41,31,26),(113,'Joint',1,41,31,26),(114,'Periscope',0,42,31,26),(115,'Stethoscope',0,42,31,26),(116,'Telescope',0,42,31,26),(117,'Microscope',1,42,31,26),(118,'Pine Trees',0,43,32,2),(119,'Palm Trees',1,43,32,2),(120,'Oak Trees',0,43,32,2),(121,'Sycamore Trees',0,43,32,2),(122,'Oak Trees',1,44,32,2),(123,'Maple Trees',1,44,32,2),(124,'Sycamore Trees',1,44,32,2),(125,'Palm Trees',0,44,32,2),(126,'Pine Tree',1,45,32,2),(127,'Palm Tree',0,45,32,2),(128,'Birch Tree',0,45,32,2),(129,'Holiday Tree',0,45,32,2),(130,'15',0,46,33,4),(131,'20',0,46,33,4),(132,'25',1,46,33,4),(133,'37',0,47,33,4),(134,'47',0,47,33,4),(135,'49',1,47,33,4),(136,'51',0,47,33,4),(137,'8',0,48,33,4),(138,'12',1,48,33,4),(139,'14',0,48,33,4),(140,'10',0,48,33,4),(141,'4',0,49,34,4),(142,'3',0,49,34,4),(143,'5',1,49,34,4),(144,'6',0,49,34,4),(145,'fifteen',0,50,34,4),(146,'17',1,50,34,4),(147,'18',0,50,34,4),(148,'seventeen',1,50,34,4),(149,'13',1,51,34,4),(150,'14',0,51,34,4),(151,'15',0,51,34,4),(152,'21',0,51,34,4),(158,'Isildur',0,54,44,5),(159,'Gandalf',0,54,44,5),(160,'Sauron',1,54,44,5),(161,'Saruman',0,54,44,5),(162,'Shire',1,55,44,5),(163,'Ard-Galen',0,55,44,5),(164,'Nevrast',0,55,44,5),(165,'Thargelion',0,55,44,5),(166,'Bilbo',0,56,44,5),(167,'Frodo',1,56,44,5),(168,'Samwise',0,56,44,5),(169,'JK Rowling',0,57,44,5),(170,'J. R. R. Tolkien',1,57,44,5),(171,'Roald Dahl',0,57,44,5),(172,'Ron',0,58,45,5),(173,'Millicent',1,58,45,5),(174,'Padma',0,58,45,5),(175,'Neville',0,58,45,5),(176,'No',0,59,45,5),(177,'Yes',1,59,45,5),(178,'Slytherin',1,60,45,5),(179,'Gryffindor',0,60,45,5),(180,'Hufflepuff',0,60,45,5),(181,'Ravenclaw',0,60,45,5),(182,'Ron and Neville',1,61,45,5),(183,'Draco and Hermione',0,61,45,5),(184,'Crabbe and Goyle',0,61,45,5),(185,'Hermione and Ron',0,61,45,5),(186,'True',1,62,46,25),(187,'False',0,62,46,25),(188,'False',0,63,46,25),(189,'True',1,63,46,25),(190,'True',0,64,46,25),(191,'False',1,64,46,25),(192,'Monopoly',0,65,47,27),(193,'Scrabble',0,65,47,27),(194,'Chess',0,65,47,27),(195,'Tic-Tac',1,65,47,27),(196,'Become Rich',0,66,47,27),(197,'Throw double sixes',0,66,47,27),(198,'Take the opponents king',1,66,47,27),(199,'Score the most',0,66,47,27),(200,'Pictionary',0,67,47,27),(201,'Yahtzee',1,67,47,27),(202,'Sorry!',1,67,47,27),(203,'Backgammon',1,67,47,27),(204,'Friction',1,68,48,25),(205,'Conduction',0,68,48,25),(206,'Radiation',0,68,48,25),(207,'Magnetism',0,68,48,25),(208,'Weight',0,69,48,25),(209,'Volume',0,69,48,25),(210,'Gravity',1,69,48,25),(211,'Electricity',0,69,48,25);
INSERT INTO `scores_per_question` VALUES (9,1,1604999790450,3,7,1,4),(10,1,1604999790450,4,7,1,4),(11,1,1605774882436,22,24,4,4),(12,1,1605774882437,23,24,4,4),(13,1,1605774882437,24,24,4,4),(14,0,1605774882437,25,24,4,4),(15,2,1605774882437,26,24,4,4),(36,1,1605799647362,22,24,4,8),(37,1,1605799647362,23,24,4,8),(38,1,1605799647362,24,24,4,8),(39,0,1605799647362,25,24,4,8),(40,2,1605799647362,26,24,4,8),(43,1,1606040314321,3,7,1,9),(44,1,1606040314321,4,7,1,9),(47,1,1606040797357,5,8,1,9),(48,1,1606040797357,6,8,1,9),(49,1,1606125876437,5,8,1,4),(50,1,1606125876437,6,8,1,4),(51,1,1606125957996,22,24,4,9),(52,1,1606125957996,23,24,4,9),(53,1,1606125957996,24,24,4,9),(54,1,1606125957996,25,24,4,9),(55,2,1606125957996,26,24,4,9),(56,1,1606911170621,46,33,4,9),(57,1,1606911170621,47,33,4,9),(58,2,1606911170621,48,33,4,9),(59,1,1606912008568,3,7,1,10),(60,1,1606912008568,4,7,1,10),(61,1,1606912116858,22,24,4,10),(62,1,1606912116858,23,24,4,10),(63,1,1606912116858,24,24,4,10),(64,1,1606912116858,25,24,4,10),(65,2,1606912116858,26,24,4,10),(66,2,1606913311114,40,31,26,4),(67,2,1606913311114,41,31,26,4),(68,2,1606913311114,42,31,26,4),(69,1,1606913547561,46,33,4,10),(70,1,1606913547561,47,33,4,10),(71,2,1606913547561,48,33,4,10),(72,1,1606917532156,3,7,1,11),(73,0,1606917532156,4,7,1,11),(74,1,1606917584189,5,8,1,11),(75,1,1606917584189,6,8,1,11),(76,1,1606917792122,19,20,2,11),(77,1,1606917792122,20,20,2,11),(78,1,1606917792122,21,20,2,11),(79,1,1606919869809,43,32,2,11),(80,2,1606919869810,44,32,2,11),(81,1,1606919869810,45,32,2,11),(82,1,1606927195167,3,7,1,16),(83,1,1606927195167,4,7,1,16),(84,1,1606927242385,46,33,4,16),(85,1,1606927242385,47,33,4,16),(86,2,1606927242385,48,33,4,16),(87,1,1606927278510,65,47,27,16),(88,1,1606927278510,66,47,27,16),(89,2,1606927278510,67,47,27,16),(90,2,1606927327592,40,31,26,16),(91,2,1606927327592,41,31,26,16),(92,2,1606927327592,42,31,26,16),(93,2,1606927375423,62,46,25,10),(94,2,1606927375423,63,46,25,10),(95,2,1606927375423,64,46,25,10),(96,1,1606927410726,54,44,5,9),(97,1,1606927410726,55,44,5,9),(98,1,1606927410726,56,44,5,9),(99,1,1606927410726,57,44,5,9),(100,2,1606927509931,62,46,25,9),(101,0,1606927509931,63,46,25,9),(102,2,1606927509931,64,46,25,9),(103,1,1606927807877,46,33,4,17),(104,0,1606927807877,47,33,4,17),(105,2,1606927807877,48,33,4,17),(106,1,1606927876806,3,7,1,17),(107,1,1606927876806,4,7,1,17),(108,1,1606927886735,5,8,1,17),(109,1,1606927886735,6,8,1,17),(110,1,1606928403532,3,7,1,18),(111,1,1606928403532,4,7,1,18),(112,1,1606928412266,5,8,1,18),(113,1,1606928412266,6,8,1,18),(114,1,1606928516436,65,47,27,18),(115,1,1606928516436,66,47,27,18),(116,0,1606928516436,67,47,27,18),(117,1,1606928544170,22,24,4,18),(118,1,1606928544170,23,24,4,18),(119,1,1606928544170,24,24,4,18),(120,1,1606928544170,25,24,4,18),(121,2,1606928544170,26,24,4,18),(122,1,1606928573661,49,34,4,18),(123,1,1606928573661,50,34,4,18),(124,2,1606928573661,51,34,4,18),(125,1,1606928649690,3,7,1,19),(126,1,1606928649690,4,7,1,19),(127,1,1606928665946,37,30,1,19),(128,1,1606928665946,38,30,1,19),(129,1,1606928665946,39,30,1,19),(130,1,1606928861497,43,32,2,20),(131,2,1606928861497,44,32,2,20),(132,1,1606928861497,45,32,2,20),(133,1,1606929032816,3,7,1,22),(134,1,1606929032816,4,7,1,22),(135,1,1606929060669,37,30,1,22),(136,1,1606929060669,38,30,1,22),(137,1,1606929060669,39,30,1,22),(138,2,1606930459992,68,48,25,4),(139,2,1606930459992,69,48,25,4),(140,2,1606930570963,40,31,26,8),(141,2,1606930570963,41,31,26,8),(142,2,1606930570963,42,31,26,8),(143,1,1606930639567,3,7,1,8),(144,1,1606930639568,4,7,1,8),(145,1,1606930882446,3,7,1,23),(146,1,1606930882446,4,7,1,23),(147,1,1606931412823,65,47,27,23),(148,1,1606931412823,66,47,27,23),(149,2,1606931412823,67,47,27,23),(150,2,1606931424311,62,46,25,23),(151,2,1606931424311,63,46,25,23),(152,0,1606931424311,64,46,25,23),(153,1,1606931809218,3,7,1,24),(154,1,1606931809218,4,7,1,24),(155,2,1606931917855,62,46,25,24),(156,2,1606931917855,63,46,25,24),(157,2,1606931917855,64,46,25,24),(158,1,1606932531003,3,7,1,25),(159,1,1606932531003,4,7,1,25),(160,0,1606933335730,58,45,5,26),(161,0,1606933335730,59,45,5,26),(162,1,1606933335730,60,45,5,26),(163,1,1606933335730,61,45,5,26),(164,1,1606933742754,19,20,2,28),(165,1,1606933742754,20,20,2,28),(166,1,1606933742754,21,20,2,28),(167,1,1606934078551,37,30,1,4),(168,1,1606934078551,38,30,1,4),(169,1,1606934078551,39,30,1,4);
