-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema quizdbnew
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema quizdbnew
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `quizdbnew` DEFAULT CHARACTER SET utf8 ;
USE `quizdbnew` ;

-- -----------------------------------------------------
-- Table `quizdbnew`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdbnew`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `category_name` VARCHAR(45) NOT NULL,
  `category_photo` VARCHAR(265) NULL DEFAULT '1606061988531_298456413.jpg',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 28
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quizdbnew`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdbnew`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `password` VARCHAR(250) NOT NULL,
  `role` VARCHAR(45) NOT NULL DEFAULT 'student',
  `has_active_quiz` TINYINT(4) NULL DEFAULT 0,
  `avatar` VARCHAR(250) NULL DEFAULT '1605578525021_344234547.jpg',
  `active_quiz` INT(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quizdbnew`.`quizzes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdbnew`.`quizzes` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `quiz_name` VARCHAR(45) NOT NULL DEFAULT 'quizname',
  `category_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `category_id`, `users_id`),
  INDEX `fk_quizzes_categories_idx` (`category_id` ASC) VISIBLE,
  INDEX `fk_quizzes_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_quizzes_categories`
    FOREIGN KEY (`category_id`)
    REFERENCES `quizdbnew`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_quizzes_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `quizdbnew`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 48
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quizdbnew`.`questions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdbnew`.`questions` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `question` VARCHAR(250) NOT NULL DEFAULT 'question',
  `points` INT(11) NOT NULL DEFAULT 0,
  `quiz_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  `q_type` TINYINT(4) NULL DEFAULT 0,
  PRIMARY KEY (`id`, `quiz_id`, `category_id`),
  INDEX `fk_questions_quizzes1_idx` (`quiz_id` ASC, `category_id` ASC) VISIBLE,
  CONSTRAINT `fk_questions_quizzes1`
    FOREIGN KEY (`quiz_id` , `category_id`)
    REFERENCES `quizdbnew`.`quizzes` (`id` , `category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 68
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quizdbnew`.`answers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdbnew`.`answers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `answer` VARCHAR(250) NOT NULL DEFAULT 'null',
  `is_correct` TINYINT(4) NOT NULL DEFAULT 0,
  `question_id` INT(11) NOT NULL,
  `quiz_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `question_id`, `quiz_id`, `category_id`),
  INDEX `fk_answers_questions_idx` (`question_id` ASC, `quiz_id` ASC, `category_id` ASC) VISIBLE,
  CONSTRAINT `fk_answers_questions`
    FOREIGN KEY (`question_id` , `quiz_id` , `category_id`)
    REFERENCES `quizdbnew`.`questions` (`id` , `quiz_id` , `category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 204
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quizdbnew`.`scores_per_question`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdbnew`.`scores_per_question` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `student_score` INT(11) NOT NULL DEFAULT 0,
  `date_taken` BIGINT(20) NULL DEFAULT NULL,
  `question_id` INT(11) NOT NULL,
  `quiz_id` INT(11) NOT NULL,
  `category_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `question_id`, `quiz_id`, `category_id`, `users_id`),
  INDEX `fk_scores_questions_questions1_idx` (`question_id` ASC) VISIBLE,
  INDEX `fk_scores_questions_quizzes1_idx` (`quiz_id` ASC, `category_id` ASC) VISIBLE,
  INDEX `fk_scores_per_question_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_scores_per_question_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `quizdbnew`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_scores_questions_questions1`
    FOREIGN KEY (`question_id`)
    REFERENCES `quizdbnew`.`questions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_scores_questions_quizzes1`
    FOREIGN KEY (`quiz_id` , `category_id`)
    REFERENCES `quizdbnew`.`quizzes` (`id` , `category_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 82
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quizdbnew`.`token_blacklist`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `quizdbnew`.`token_blacklist` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `token` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
