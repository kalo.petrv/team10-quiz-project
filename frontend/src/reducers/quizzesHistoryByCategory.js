const quizzesHistoryByCategory = (history = [], action) => {
  switch (action.type) {
    case 'HISTORY_CATEGORY':
      return action.payload;
      case 'RESET_HISTORY_CATEGORY':
      return [];
    default:
      return history;
  }
};

export default quizzesHistoryByCategory;
