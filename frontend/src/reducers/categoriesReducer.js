const categoriesReducer = (categories = [], action) => {
  switch (action.type) {
    case 'ADD_CATEGORY':
      return [...categories, action.payload];
      case 'ALL_CATEGORIES':
        return [...action.payload];
        case 'RESET_CATEGORIES':
        return [];
    default:
      return categories;
  }
};

export default categoriesReducer;
