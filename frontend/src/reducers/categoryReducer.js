const categoryReducer = (category = {}, action) => {
    switch (action.type) {
        case 'SINGLE_CATEGORY': 
        return action.payload;
      default:
        return category;
    }
  };
  
  export default categoryReducer;
  