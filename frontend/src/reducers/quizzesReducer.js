const quizzesReducer = (quizzes = [], action) => {
  switch (action.type) {
    case 'ADD_QUIZ':
      return [...quizzes, action.payload];
    case 'ALL_QUIZZES':
      return [...action.payload];
      case 'RESET_QUIZZES':
        return [...action.payload];
    default:
      return quizzes;
  }
};

export default quizzesReducer;
