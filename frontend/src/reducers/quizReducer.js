const quizReducer = (quiz = {}, action) => {
  switch (action.type) {
    case 'SINGLE_QUIZ':
      return action.payload;
    case 'CHOOSE_CATEGORY':
      const withNewCategory = { ...quiz }
      withNewCategory.quiz_details.category_name = action.payload
      return withNewCategory;
    case 'ADD_QUESTION':
      return { ...quiz }.quiz_details.questions.push(action.payload);
    case 'CHANGE_QUESTION_TYPE':
      const changedQuiz = { ...quiz };
      if (changedQuiz.quiz_details.questions[action.payload].type === 0) {
        changedQuiz.quiz_details.questions[action.payload].type = 1;
      } else if (
        changedQuiz.quiz_details.questions[action.payload].type === 1
      ) {
        changedQuiz.quiz_details.questions[action.payload].type = 0;
        changedQuiz.quiz_details.questions[action.payload].answers.map((a) => {
          a.is_correct = 0;
          return a;
        });
      }
      return changedQuiz;
    case 'REMOVE_QUESTION':
      return { ...quiz }.quiz_details.questions.pop();
    case 'ADD_АNSWER':
      return { ...quiz }.quiz_details.questions[
        action.payload.question_index
      ].answers.push(action.payload.answer);
    case 'REMOVE_АNSWER':
      return { ...quiz }.quiz_details.questions[
        action.payload
      ].answers.pop()

    case 'CHANGE_ANSWER_OPTIONS_SINGLE':
      const optionChanged = { ...quiz };
      if (
        optionChanged.quiz_details.questions[action.payload.question_id]
          .answers[action.payload.answer_id].is_correct === 0
      ) {
        optionChanged.quiz_details.questions[
          action.payload.question_id
        ].answers[action.payload.answer_id].is_correct = 1;

        optionChanged.quiz_details.questions[
          action.payload.question_id
        ].answers.map((a, i) => {
          if (i !== action.payload.answer_id) a.is_correct = 0;
          return a;
        });
      } else {
        optionChanged.quiz_details.questions[
          action.payload.question_id
        ].answers[action.payload.answer_id].is_correct = 0;
      }
      return optionChanged;
    case 'CHANGE_ANSWER_OPTIONS_MULTIPLE':
      const optionChangedMultiple = { ...quiz };
      if (
        optionChangedMultiple.quiz_details.questions[action.payload.question_id]
          .answers[action.payload.answer_id].is_correct == 0
      ) {
        optionChangedMultiple.quiz_details.questions[
          action.payload.question_id
        ].answers[action.payload.answer_id].is_correct = 1;
      } else {
        optionChangedMultiple.quiz_details.questions[
          action.payload.question_id
        ].answers[action.payload.answer_id].is_correct = 0;
      }

      return optionChangedMultiple;
    default:
      return quiz;
  }
};

export default quizReducer;
