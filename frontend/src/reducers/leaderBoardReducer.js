const leaderBoardReducer = (leaderBoard = [], action) => {
  switch (action.type) {
    case 'LEADERBOARD':
      return action.payload;
    case 'ALL_SCORES_PER_QUIZ':
      return action.payload;
    default:
      return leaderBoard;
  }
};

export default leaderBoardReducer;
