import categoriesReducer from './categoriesReducer';
import quizzesReducer from './quizzesReducer';
import { combineReducers } from 'redux';
import quizReducer from './quizReducer';
import categoryReducer from './categoryReducer';
import leaderBoardReducer from './leaderBoardReducer'
import lastQuizesHistoryReducer from './lastQuizesHistoryReducer';
import quizzesHistoryByCategory from './quizzesHistoryByCategory';
import userActiveQuiz from './userActiveQuiz';
import userFullHistoryReducer from './userFullHistoryReducer';
import fullLeaderboardReducer from './fullLeaderboardReducer';

const allReducers = combineReducers({
    categories: categoriesReducer,
    category: categoryReducer,
    quizzes: quizzesReducer,
    quiz: quizReducer,
    leaderboard: leaderBoardReducer,
    lastfivehistory: lastQuizesHistoryReducer,
    historyCategory: quizzesHistoryByCategory,
    activeQuiz:userActiveQuiz,
    fullHistory:userFullHistoryReducer,
    fullLeaderboardReducer:fullLeaderboardReducer
})

export default allReducers;