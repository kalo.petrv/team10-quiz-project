const fullLeaderboardReducer = (leaderBoard = [], action) => {
    switch (action.type) {
      case 'FULL_LEADERBOARD':
        return action.payload;
      default:
        return leaderBoard;
    }
  };
  
  export default fullLeaderboardReducer;