const lastQuizesHistoryReducer = (history = [], action) => {
    switch (action.type) {
        case 'LAST_FIVE_QUIZZES': 
        return action.payload;
      default:
        return history;
    }
  };
  
  export default lastQuizesHistoryReducer;
  