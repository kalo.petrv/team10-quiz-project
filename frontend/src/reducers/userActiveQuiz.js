const userActiveQuiz = (quizzes = 0, action) => {
    switch (action.type) {
      case 'ADD_QUIZ':
        return action.payload;
      case 'REMOVE_QUIZZES':
        return action.payload;
      default:
        return quizzes;
    }
  };
  
  export default userActiveQuiz;