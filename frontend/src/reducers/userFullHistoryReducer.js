const userFullHistoryReducer = (history = [], action) => {
    switch (action.type) {
        case 'HISTORY_QUIZZES': 
        return action.payload;
      default:
        return history;
    }
  };
  
  export default userFullHistoryReducer;
  