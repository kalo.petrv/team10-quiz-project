export const addCategory = (category) => {
  return {
    type: 'ADD_CATEGORY',
    payload: category,
  };
};

export const getAllCategories = (categories) => {
  return {
    type: 'ALL_CATEGORIES',
    payload: categories,
  };
};

export const resetCategories = (categories) => {
  return {
    type: 'RESET_CATEGORIES',
    payload: categories,
  };
};

export const getSingleCategory = (category) => {
  return {
    type: 'SINGLE_CATEGORY',
    payload: category,
  };
};
export const getAllQuizzes = (quizzes) => {
  return {
    type: 'ALL_QUIZZES',
    payload: quizzes,
  };
};

export const resetQuizzes = (quizzes) => {
  return {
    type: 'ALL_QUIZZES',
    payload: quizzes,
  };
};

export const getSingleQuiz = (quiz) => {
  return {
    type: 'SINGLE_QUIZ',
    payload: quiz,
  };
};

export const addQuiz = (quiz) => {
  return {
    type: 'ADD_QUIZ',
    payload: { ...quiz },
  };
};

export const addQuestion = (question) => {
  const answer1 = { ...question.answers[0] };
  const answer2 = { ...question.answers[1] };
  const newQuestion = { ...question };
  newQuestion.answers = [];
  newQuestion.answers.push(answer1);
  newQuestion.answers.push(answer2);
  return {
    type: 'ADD_QUESTION',
    payload: newQuestion,
  };
};

export const removeQuestion = () => {
  return {
    type: 'REMOVE_QUESTION'
  };
};

export const questionType = (question_id) => {
  return {
    type: 'CHANGE_QUESTION_TYPE',
    payload: question_id,
  };
};

export const answerOptionsChange = (question_id, answer_id, question_type) => {
  if (question_type === 0) {
    return {
      type: 'CHANGE_ANSWER_OPTIONS_SINGLE',
      payload: {
        question_id,
        answer_id,
      },
    };
  }
  return {
    type: 'CHANGE_ANSWER_OPTIONS_MULTIPLE',
    payload: {
      question_id,
      answer_id,
    },
  };
};

export const addAnswer = (question_index, answer) => {
  return {
    type: 'ADD_АNSWER',
    payload: {
      answer: { ...answer },
      question_index,
    },
  };
};

export const removeAnswer = (question_id) => {
  return {
    type: 'REMOVE_АNSWER',
    payload: question_id,
  };
};

export const viewLeaderBoard = (leaderboard) => {
  return {
    type: 'LEADERBOARD',
    payload: leaderboard,
  };
};

export const scoresPerQuiz = (scores) => {
  return {
    type: 'ALL_SCORES_PER_QUIZ',
    payload: scores,
  };
};

export const getLastFiveQuizesHisroy = (history) => {
  return {
    type: 'LAST_FIVE_QUIZZES',
    payload: history,
  };
};

export const chooseCategory = (category) => {
  return {
    type: 'CHOOSE_CATEGORY',
    payload: category,
  };
};

export const getHistoryFromCategory = (history) => {
  return {
    type: 'HISTORY_CATEGORY',
    payload: history,
  };
};

export const resetHistoryFromCategory = (history) => {
  return {
    type: 'RESET_HISTORY_CATEGORY',
    payload: history,
  };
};

export const addActiveQuiz = (quiz) => {
  return {
    type: 'ADD_QUIZ',
    payload: quiz,
  };
};

export const removeActiveQuiz = () => {
  return {
    type: 'REMOVE_QUIZ',
    payload: 0,
  };
};

export const getFullHistory = (history) => {
  return {
    type: 'HISTORY_QUIZZES',
    payload: history,
  };
};

export const setFullLeaderboard = (leaderboard) => {
  return {
    type: 'FULL_LEADERBOARD',
    payload: leaderboard,
  };
};