import React, { useState } from 'react';
import './App.css';
import TeacherDashboard from './components/TeacherDashboard/TeacherDashboard';
import StudentDashboard from './components/StudentDashboard/StudentDashboard';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Header from './components/Base/Header/Header.js';
import LogInPage from './components/AuthPages/LogInPage';
import RegisterPage from './components/AuthPages/RegisterPage';
import Home from './components/Pages/Home/Home';
import AuthContext, { extractUser, getToken } from './components/AuthContext/AuthContext';
import AllQuizzesContainer from './containers/AllQuizzesContainer';
import ViewQuiz from './components/Quizzes/ViewQuiz/ViewQuiz';
import SolveQuizContainer from './containers/SolveQuizContainer';
import CreateQuizContainer from './containers/CreateQuizContainer';
import FulHistoryContainer from './containers/FulHistoryContainer';
import FullLeaderBoardContainer from './containers/FullLeaderBoardContainer';

function App() {
  const [authValue, setAuthValue] = useState({
    isLoggedIn: !!extractUser(getToken()),
    user: extractUser(getToken()),
  });

  return (
    <div className='App'>
      <BrowserRouter>
        <AuthContext.Provider
          value={{ ...authValue, setLoginState: setAuthValue }}
        >
          <Header />
          <div className='quiz-app'>

            {!authValue.isLoggedIn && (
              <Switch>
                <Redirect path="/" exact to="home" />
                <Route path="/home" component={Home} />
                <Route path='/signin' exact component={LogInPage} />
                <Route path='/signup' exact component={RegisterPage} />
                <Route path='/register/only-for-teachers' exact component={RegisterPage} />
                <Route path='*' component={Home}>
                  <Redirect to='/home' />
                </Route>
              </Switch>
            )}

            {authValue.user && authValue.user.role === 'teacher' ? (
              <div className='whole-app'>
                <Switch>
                  <Redirect path="/" exact to="home" />
                  <Route path="/home" component={Home} />
                  <Route path='/teacher' exact component={TeacherDashboard} />
                  <Route path='/teacher/categories/:id/quizzes' exact component={AllQuizzesContainer} />
                  <Route path='/teacher/create-quiz' exact component={CreateQuizContainer} />
                  <Route path='/teacher/categories/:id/quizzes/:quizId' exact component={SolveQuizContainer} />
                  <Route path='/teacher/categories/:id/quizzes/:quizId/scores' exact component={ViewQuiz} />
                  <Route path='*' component={Home}>
                    <Redirect to='/home' />
                  </Route>
                </Switch>

              </div>
            ) : (null
              )}

            {authValue.user && authValue.user.role === 'student' ? (
              <div className='whole-app'>
                <Switch>
                  Student
                  <Redirect path="/" exact to="home" />
                  <Route path='/home' exact component={Home} />
                  <Route path='/student/history' exact component={FulHistoryContainer} />
                  <Route path='/student/leaderboard' exact component={FullLeaderBoardContainer} />
                  <Route path='/student' exact component={StudentDashboard} />
                  <Route path='/student/categories/:id/quizzes' exact component={AllQuizzesContainer} />
                  <Route path='/student/categories/:id/quizzes/:quizId' exact component={SolveQuizContainer} />
                  <Route path='/student/categories/:id/quizzes/:quizId/scores' exact component={ViewQuiz} />
                  <Route path='*' component={Home}>
                    <Redirect to='/home' />
                  </Route>
                </Switch>
              </div>
            ) : (null)}
          </div>
        </AuthContext.Provider>
      </BrowserRouter>
    </div>
  );
}

export default App;
