import React, { useState, useEffect, useContext } from 'react';
import httpProvider from '../providers/httpProvider';
import '../components/Base/Header/Avatar.scss';
import { useSelector, useDispatch } from 'react-redux';
import { STUDENT_URL, TEACHER_URL, BASE_URL } from '../common/constants';
import { viewLeaderBoard } from '../actions';
import Swal from 'sweetalert2';

const UploadProfilePictureContainer = () => {
  const token = localStorage.getItem('token');
  const [src, setSrc] = useState('');
  let inputElement;

  const leaderboard = useSelector((state) => state.leaderboard);
  const dispatch = useDispatch();


  useEffect(() => {
    httpProvider.get(`${BASE_URL}/auth/info`).then((data) => {
      setSrc(data.avatar);
    });
    httpProvider
      .get(`${STUDENT_URL}/leaderboard/top`)
      .then((data) => {
        if (data.error) {
        } else {
          dispatch(viewLeaderBoard(data));
        }
      })
  }, [src]);

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        setSrc(e.target.result);
      };
      reader.readAsDataURL(input.files[0]);
      const formData = new FormData();
      formData.set('avatar', input.files[0]);

      if(input.files[0].name.substr(input.files[0].name.length - 4 ) !== '.jpg' && input.files[0].name.substr(input.files[0].name.length - 4) !== '.png'){
        Swal.fire({
          title: `Please provide an image file with the extention of jpg/png`,
          icon: 'eror',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'OK',
        });
      } else {
      fetch(`${BASE_URL}/api/avatar`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: formData,
      })
        .then((r) => r.json())
        .then((data) => {
        });
      }
    }
  }
  function handleOnChange(e) {
    readURL(e.target);
  }

  function handleOnClick(e) {
    inputElement.click();
  }
  return (
    <div>
      <div className='avatar-wrapper'>
        <img
          className='profile-pic'
          src={`${BASE_URL}/public/${src}`}
          alt=''
        />
        <div className='upload-button' onClick={handleOnClick}>
          <i className='fa fa-arrow-circle-up' aria-hidden='true'></i>
        </div>
        <input
          className='file-upload'
          type='file'
          accept='image/*'
          onChange={handleOnChange}
          ref={(input) => (inputElement = input)}
        />
      </div>
    </div>
  );
};

export default UploadProfilePictureContainer;
