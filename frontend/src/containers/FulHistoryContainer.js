import React, { useState, useEffect, useContext } from 'react';
import { STUDENT_URL, TEACHER_URL } from '../common/constants';
import AuthContext from '../components/AuthContext/AuthContext';
import httpProvider from '../providers/httpProvider';
import { useSelector, useDispatch } from 'react-redux';
import { getFullHistory } from '../actions';
import FullHistory from '../components/History/FullHistory/FullHistory';

const FulHistoryContainer = (props) => {
    const [error, setError] = useState(null);
    const { user } = useContext(AuthContext);

    const history = useSelector((state) => state.fullHistory);
    const dispatch = useDispatch();

    const URL = user && user.role === 'teacher' ? TEACHER_URL : STUDENT_URL;

    useEffect(() => {
        httpProvider
            .get(`${URL}/history`, { user_id: user.sub })
            .then((data) => {
                if (data.error) {
                    setError(data.error.toString());
                } else {
                    dispatch(getFullHistory(data));
                }
            })
            .catch((error) => setError(error.toString()));
    }, []);

    return (
        <div>
            <br></br>
            <br></br>
            <br></br>
            <br></br>
            <h1>Full History</h1>
            <br></br>
            <FullHistory quizzes={history}></FullHistory>
            <br />
            <button type="button" class="btn btn-secondary" onClick={() => { props.history.goBack() }}>Back To Dashboard</button>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        </div>
    );
};

export default FulHistoryContainer;
