import React, { useState, useEffect, useContext } from 'react';
import { STUDENT_URL, TEACHER_URL } from '../common/constants';
import AuthContext from '../components/AuthContext/AuthContext';
import httpProvider from '../providers/httpProvider';
import { useSelector, useDispatch } from 'react-redux';
import { viewLeaderBoard } from '../actions';
import { NavLink } from 'react-router-dom';
import LeaderBoardView from '../components/Leaderboard/LeaderBoardView';

const LeaderBoardTopUsersContainer = ({ props }) => {
  const [error, setError] = useState(null);
  const { user } = useContext(AuthContext);

  const path = user && user.role === 'teacher' ? 'teacher' : 'student';

  const leaderboard = useSelector((state) => state.leaderboard);
  const dispatch = useDispatch();

  const URL = user && user.role === 'teacher' ? TEACHER_URL : STUDENT_URL;

  useEffect(() => {
    httpProvider
      .get(`${URL}/leaderboard/top`)
      .then((data) => {
        if (data.error) {
          setError(data.error.toString());
        } else {
          dispatch(viewLeaderBoard(data));
        }
      })
      .catch((error) => setError(error.toString()));
  }, []);

  return (
    <>
      <br></br>
      <h2>Top 10 users</h2>
      <LeaderBoardView users={leaderboard} />
      <br></br>
      <NavLink to={`/${path}/leaderboard`}>
        <button type='button' class='btn btn-dark'>
          View Full Leaderboard
        </button>
      </NavLink>
    </>
  );
};

export default LeaderBoardTopUsersContainer;
