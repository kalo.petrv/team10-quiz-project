import React, { useState, useEffect, useContext } from 'react';
import { STUDENT_URL, TEACHER_URL } from '../common/constants';
import AuthContext from '../components/AuthContext/AuthContext';
import AllCategories from '../components/Categories/AllCategories/AllCategories';
import httpProvider from '../providers/httpProvider';
import { useSelector, useDispatch } from 'react-redux';
import { getAllCategories } from '../actions';

const AllCategoriesContainer = (props) => {
  const [search, setSearch] = useState('');
  const [error, setError] = useState(null);

  const { user } = useContext(AuthContext);

  const categories = useSelector((state) => state.categories);
  const dispatch = useDispatch();

  const URL = user && user.role === 'teacher' ? TEACHER_URL : STUDENT_URL;

  useEffect(() => {
    httpProvider
      .get(`${URL}/categories?search=${search}`)
      .then((data) => {
        if (data.error) {
          setError(data.error.toString());
        } else {
          dispatch(getAllCategories(data))
        }
      })
      .catch((error) => setError(error.toString()));
  }, []);


  return (
    <div>
      <h2>Quiz Categories</h2>
      <AllCategories categories={categories} />
    </div>
  )
};

export default AllCategoriesContainer;
