import React, { useState, useEffect, useContext } from 'react';
import SolveQuiz from '../components/Quizzes/SolveQuiz/SolveQuiz';
import { STUDENT_URL, TEACHER_URL } from '../common/constants';
import AuthContext from '../components/AuthContext/AuthContext';
import httpProvider from '../providers/httpProvider';
import { useSelector, useDispatch } from 'react-redux';
import { getSingleQuiz, addActiveQuiz  } from '../actions';
import Swal from 'sweetalert2';

const SolveQuizContainer = (props) => {
  const [error, setError] = useState(null);
  const { user } = useContext(AuthContext);
  const { id, quizId } = props.match.params;
  const path = user && user.role === 'teacher' ? 'teacher' : 'student';

  const quiz = useSelector((state) => state.quiz);
  const dispatch = useDispatch();

  const quizBody = {
    user_id: user.sub,
    answers: [],
  };
  const answersBody = {
    question_id: null,
    answers_IDS: [],
  };

  const URL = user && user.role === 'teacher' ? TEACHER_URL : STUDENT_URL;

  useEffect(() => {
    httpProvider
      .get(`${URL}/categories/${id}/quizzes/${quizId}`)
      .then((data) => {
        if (data.error) {
          return null;
        } else {
          dispatch(getSingleQuiz(data));
        }
      })
      .catch((error) => setError(error.toString()));
  }, []);

  const submitSolvedQuiz = (ev, solvedQuiz) => {
    ev.preventDefault();
    solvedQuiz.user_id = user.sub;
    const answers = solvedQuiz.answers;
    const count =  answers.reduce((acc, element) => {
      console.log(element.answers_IDS)
      if(element.answers_IDS.length > 0) {
        return acc+=1
      } else {
        return acc
      }
    },0);
    console.log(count)

    if(user.role === 'student' && +answers.length !== +count ){
      Swal.fire({
        title: `Continue the quiz`,
        text: `You have empty answers`,
        icon: 'info',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'OK',
      });
    } else {
      httpProvider
      .post(`${URL}/categories/${id}/quizzes/${quizId}/solutions`, solvedQuiz)
      .then((data) => {
        if (data.error) {
          setError(data.error.toString());
        } else {
          dispatch(addActiveQuiz(0))
          Swal.fire({
            title: `Your Score is: ${data.score}/${data.possiblePoints} 
              for Quiz: ${quiz?.quiz_details[0]?.quiz_name}`,
            icon: 'info',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK',
          });
          props.history.goBack();
        }
      })
      .catch((error) => setError(error.toString()));
    }
    
  
  };

  return error ? (
    <p>
      <br />
      <br />
      <br />
      <br />
      <button
        className='btn btn-secondary'
        onClick={(e) => {
          e.preventDefault();
          props.history.goBack();
        }}
      >
        Go Back
      </button>{' '}
      <br />
      {error}
    </p>
  ) : (
    <div>
      <br />
      <br />
      {user.role === 'teacher' ? (
        <div>
          <br />
          <button
            className='btn btn-secondary'
            onClick={(e) => {
              e.preventDefault();
              props.history.goBack();
            }}
          >
            Go Back
          </button>
          <br />
        </div>
      ) : (
        ''
      )}

      <br />
      <SolveQuiz
        quiz={quiz}
        quizBody={quizBody}
        answersBody={answersBody}
        submitSolvedQuiz={submitSolvedQuiz}
      />
      <br/>
      <br/>
      <br/>
      <br/>
    </div>
  );
};

export default SolveQuizContainer;
