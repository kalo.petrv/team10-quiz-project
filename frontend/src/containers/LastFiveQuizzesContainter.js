import React, { useState, useEffect, useContext } from 'react';
import { STUDENT_URL, TEACHER_URL } from '../common/constants';
import AuthContext from '../components/AuthContext/AuthContext';
import AllLastFiveQuizzes from '../components/History/LastFiveQuizzes/AllLastFiveQuizzes/AllLastFiveQuizzes';
import httpProvider from '../providers/httpProvider';
import { useSelector, useDispatch } from 'react-redux';
import { getLastFiveQuizesHisroy } from '../actions';
import { NavLink } from 'react-router-dom';

const LastFiveQuizzesContainter = (props) => {
    const [error, setError] = useState(null);
    const { user } = useContext(AuthContext);
    const path = user && user.role === 'teacher' ? 'teacher' : 'student';

    const historyQuizzes = useSelector((state) => state.lastfivehistory);
    const dispatch = useDispatch();

    const URL = user && user.role === 'teacher' ? TEACHER_URL : STUDENT_URL;

    useEffect(() => {
        httpProvider
            .get(`${URL}/history/last`, { user_id: user.sub })
            .then((data) => {
                if (data.error) {
                    setError(data.error.toString());
                } else {
                    dispatch(getLastFiveQuizesHisroy(data));
                }
            })
            .catch((error) => setError(error.toString()));
    }, []);


    return (
        <div>
            <h2>Recently Solved Quizzes</h2>
            <AllLastFiveQuizzes quizzes={historyQuizzes}></AllLastFiveQuizzes>
            <NavLink to={`/${path}/history`}>
                <button type="button" class="btn btn-dark" >View Full History</button>
            </NavLink>
            <br></br>
        </div>

    );
};


export default LastFiveQuizzesContainter;
