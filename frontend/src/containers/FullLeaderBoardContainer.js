import React, { useState, useEffect, useContext } from 'react';
import { STUDENT_URL, TEACHER_URL } from '../common/constants';
import AuthContext from '../components/AuthContext/AuthContext';
import httpProvider from '../providers/httpProvider';
import { useSelector, useDispatch } from 'react-redux';
import { viewLeaderBoard, setFullLeaderboard } from '../actions';
import LeaderBoardView from '../components/Leaderboard/LeaderBoardView';
import FormControl from 'react-bootstrap/FormControl';

const FullLeaderBoardContainer = (props) => {
  const [error, setError] = useState(null);
  const { user } = useContext(AuthContext);
  const [search, setSearch] = useState('');


  const leaderboard = useSelector((state) => state.fullLeaderboardReducer);
  const dispatch = useDispatch();

  const URL = user && user.role === 'teacher' ? TEACHER_URL : STUDENT_URL;

  useEffect(() => {
    httpProvider
      .get(`${URL}/leaderboard?search=${search}`)
      .then((data) => {
        if (data.error) {
          dispatch(setFullLeaderboard([]));
          setError(data.error.toString());
        } else {
          dispatch(setFullLeaderboard(data));
        }
      })
      .catch((error) => setError(error.toString()));
  }, [search]);

  return (
    <div>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <h1>Student's Leaderboard</h1>
      <br></br>
      <div style={{
        alignItems: 'center',
        justifyContent: 'center',
        position: 'center',
        margin: 'auto',
        width: "40%"
      }}>
        <FormControl
          type='text'
          placeholder='Search'
          className='mr-sm-2'
          onChange={(e) => setSearch(e.target.value)}
        />
      </div>
      <br></br>
      <LeaderBoardView users={leaderboard} />
      <br />
      <br />
      <button
        type='button'
        class='btn btn-secondary'
        onClick={() => {
          props.history.goBack();
        }}
      >
        Back To Dashboard
      </button>
      <br />
      <br />
    </div>
  );
};

export default FullLeaderBoardContainer;
