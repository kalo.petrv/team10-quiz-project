import React, { useContext, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllCategories } from '../actions';
import { TEACHER_URL } from '../common/constants';
import AuthContext from '../components/AuthContext/AuthContext';
import CreateQuiz from '../components/Quizzes/CreateQuiz/CreateQuiz';
import httpProvider from '../providers/httpProvider';
import Swal from 'sweetalert2';

const CreateQuizContainer = (props) => {
  const { user } = useContext(AuthContext);
  const categories = useSelector((state) => state.categories);
  const dispatch = useDispatch();

  useEffect(() => {
    httpProvider.get(`${TEACHER_URL}/categories`).then((data) => {
      dispatch(getAllCategories(data));
    });
  }, []);

  const quizBody = {
    quiz_details: {
      name: '',
      category_name: '',
      questions: [
        {
          question: '',
          points: 0,
          type: 0,
          id: 0,
          answers: [
            {
              answer: '',
              is_correct: 0,
            },
            {
              answer: '',
              is_correct: 0,
            },
          ],
        },
        {
          question: '',
          points: 0,
          type: 0,
          id: 1,
          answers: [
            {
              answer: '',
              is_correct: 0,
            },
            {
              answer: '',
              is_correct: 0,
            },
          ],
        },
      ],
    },
  };

  const answerBody = {
    answer: '',
    is_correct: 0,
  };

  const questionBody = {
    question: '',
    points: 0,
    type: 0,
    id: 2,
    answers: [
      {
        answer: '',
        is_correct: 0,
      },
      {
        answer: '',
        is_correct: 0,
      },
    ],
  };
  const createQuiz = (ev, quiz) => {
    ev.preventDefault();

    httpProvider
      .post(`${TEACHER_URL}/quizzes`, { quiz, user_id: user.sub })
      .then((data) => {
        if (data.error) {
          return Swal.fire({
            title: `${data.error}`,
            icon: 'error',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK'
          })
        } else {
          Swal.fire({
            title: 'Quiz Created Successfully',
            icon: 'success',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK'
          })
          props.history.goBack();
        }
      });
  };

  return categories.length >= 1 ? (
    <div>
      <br />
      <br />
      <CreateQuiz
        createQuiz={createQuiz}
        quizBody={quizBody}
        answerBody={answerBody}
        questionBody={questionBody}
        categories={categories}
      />
    </div>
  ) : (
      <div>...</div>
    );
};

export default CreateQuizContainer;
