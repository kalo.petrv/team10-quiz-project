import React, { useState, useEffect, useContext } from 'react';
import { STUDENT_URL, TEACHER_URL, BASE_URL } from '../common/constants';
import AuthContext from '../components/AuthContext/AuthContext';
import httpProvider from '../providers/httpProvider';
import { useSelector, useDispatch } from 'react-redux';
import {
  getAllQuizzes,
  getSingleCategory,
  resetQuizzes,
  resetHistoryFromCategory,
} from '../actions';
import AllQuizzes from '../components/Quizzes/AllQuizzes/AllQuizzes';
import CategoryHistoryContainer from './CategoryHistoryContainer';
import UpdatePhoto from '../components/Categories/UpdatePhoto/UpdatePhoto';

const AllQuizzesContainer = (props) => {
  const [error, setError] = useState(null);
  const { user } = useContext(AuthContext);
  const categoryId = props.match.params.id;

  const quizzes = useSelector((state) => state.quizzes);
  const category = useSelector((state) => state.category);
  const dispatch = useDispatch();

  const URL = user && user.role === 'teacher' ? TEACHER_URL : STUDENT_URL;
  const path = user && user.role === 'teacher' ? 'teacher' : 'student';

  useEffect(() => {
    httpProvider
      .get(`${URL}/categories/${categoryId}/quizzes`)
      .then((data) => {
        if (data.error) {
          dispatch(getAllQuizzes([]));
          setError(data.error.toString());
        } else {
          dispatch(getAllQuizzes(data));
        }
      })
      .catch((error) => setError(error.toString()));

    httpProvider
      .get(`${URL}/categories`)
      .then((data) => {
        if (data.error) {
          setError(data.error.toString());
        } else {
          const obj = data.filter((c) => c.category_id == categoryId);
          dispatch(getSingleCategory(obj[0]));
        }
      })
      .catch((error) => setError(error.toString()));
  }, []);

  return (
    <div>
      <br></br>
      <br></br>
      <br></br>
      <button
        className='btn btn-secondary'
        onClick={() => {
          props.history.push(`/${path}`);
          dispatch(resetQuizzes([]));
          dispatch(resetHistoryFromCategory([]));
        }}
      >
        Back To Dashboard
      </button>
      <br></br>
      <h1>Category: {category.category_name}</h1>
      <img
        src={`${BASE_URL}/public/${category.category_photo}`}
        style={{
          height: '200px',
          width: '280px',
          borderRadius: '5px',
        }}
      />
      <br />
      <h2>Available Quizzes</h2>
      {quizzes.length === 0 ? (
        error
      ) : (
          <div style={{ width: '40%', margin: 'auto' }}>
            <AllQuizzes quizzes={quizzes} category={category} />
          </div>
        )}
      <br></br>
      {categoryId && user.role === 'student' ? (
        <>
          <h2>My Already Solved Quizzes:</h2>
          <CategoryHistoryContainer
            categoryId={categoryId}
          ></CategoryHistoryContainer>
        </>
      ) : (
          <UpdatePhoto categoryId={categoryId} />
        )}
      <br />

      <br></br>
      <br></br>
    </div>
  );
};

export default AllQuizzesContainer;
