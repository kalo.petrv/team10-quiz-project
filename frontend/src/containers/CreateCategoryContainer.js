import React from 'react';
import { TEACHER_URL } from '../common/constants';
import CreateCategory from '../components/Categories/CreateCategory/CreateCategory';
import httpProvider from '../providers/httpProvider';
import { useDispatch } from 'react-redux';
import { addCategory } from '../actions';
import Swal from 'sweetalert2';

const CreateCategoryContainer = (props) => {
  const dispatch = useDispatch();

  const createCategory = (e, category) => {
    e.preventDefault();

    httpProvider
      .post(`${TEACHER_URL}/categories`, {
        category_name: category,
      })
      .then((data) => {
        if (data.error) {
          Swal.fire({
            title: `${data.error}`,
            icon: 'error',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Confirm'
          })
        } else {
          Swal.fire({
            title: `New category is added`,
            icon: 'success',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Confirm'
          })
          dispatch(addCategory(data.newCategory[0]));
        }
      });
  };

  return (
    <div>
      <CreateCategory createCategory={createCategory} props={props} />
    </div>
  );
};

export default CreateCategoryContainer;
