import React, { useState, useEffect, useContext } from 'react';
import { STUDENT_URL } from '../common/constants';
import AuthContext from '../components/AuthContext/AuthContext';
import httpProvider from '../providers/httpProvider';
import { useSelector, useDispatch } from 'react-redux';
import { getHistoryFromCategory } from '../actions';
import HistoryByCategory from '../components/History/HistoryByCategory/HistoryByCategory';

const CategoryHistoryContainer = ({ categoryId }) => {
    const [error, setError] = useState(null);
    const { user } = useContext(AuthContext);

    const history = useSelector((state) => state.historyCategory);
    const dispatch = useDispatch();

    useEffect(() => {
        httpProvider
            .get(`${STUDENT_URL}/categories/${categoryId}/history`)
            .then((data) => {
                if (data.error) {
                    dispatch(getHistoryFromCategory([]));
                    setError(data.error.toString());
                } else {
                    dispatch(getHistoryFromCategory(data));
                }
            })
            .catch((error) => setError(error.toString()));
    }, [categoryId]);

    return (
        <div style={{ width: "60%", margin: 'auto'}}>
            <HistoryByCategory quizzes={history}></HistoryByCategory>
        </div>
    );
};

export default CategoryHistoryContainer;
