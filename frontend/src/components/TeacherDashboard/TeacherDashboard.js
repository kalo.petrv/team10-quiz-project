import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import AllCategoriesContainer from '../../containers/AllCategoriesContainer';
import CreateCategoryContainer from '../../containers/CreateCategoryContainer';
import TeacherLast5Quizzes from '../Quizzes/TeacherLast5Quizzes/TeacherLast5Quizzes';
import './TeacherDashboard.css';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import HeroImage2 from '../Pages/Home/HeroSection/HeroImage/quiz2.jpg';

const TeacherDashboard = () => {
  const categories = useSelector((state) => state.categories);

  return (

    <div className='teacher-dashboard'>
      <br></br>
      <h2>Teacher's Dashboard</h2>
      <br />
      {categories.length >= 1 ? (
        <NavLink to='/teacher/create-quiz'>
          <button type='button' className='btn btn-primary btn-lg'>
            Create Quiz
          </button>
        </NavLink>
      ) : (
          <button disabled>Create Quiz</button>
        )}
      <Container>
        <br></br>
        <Row>
          <Col>
          <br/>
            <AllCategoriesContainer />
          </Col>
          <Col>
            <br />
            <br />
            <br />
            <br />
            <CreateCategoryContainer />
            <br />
            <br />
            <TeacherLast5Quizzes />
          </Col>
        </Row>
      </Container>
      <br></br>
      <br></br>
    </div>
  
  );
};

export default TeacherDashboard;
