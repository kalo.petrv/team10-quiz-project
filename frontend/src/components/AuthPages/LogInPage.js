import React, { useState, useContext } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import AuthContext, {
  extractUser,
  assignURL,
} from '../AuthContext/AuthContext';
import Form from 'react-bootstrap/Form';
import './LogInPage.css';
import Swal from 'sweetalert2';
import { useDispatch } from 'react-redux';
import HeroImage2 from '../Pages/Home/HeroSection/HeroImage/quiz2.jpg';
import { addActiveQuiz, removeActiveQuiz } from './../../actions';
import { BASE_URL } from '../../common/constants';

const required = (value) => value.trim().length >= 1;
const minLen = (len) => (value) => value.trim().length >= len;
const maxLen = (len) => (value) => value.trim().length <= len;
const LogInPage = (props) => {
  const dispatch = useDispatch();
  const [usernameControl, setUsernameControl] = useState({
    value: '',
    valid: false,
    validators: [required, minLen(5), maxLen(20)],
  });
  const [passwordControl, setPasswordControl] = useState({
    value: '',
    valid: false,
    validators: [required, minLen(5), maxLen(30)],
  });
  const [loading, setLoading] = useState(false);
  const { isLoggedIn, setLoginState } = useContext(AuthContext);

  const validate = (value, validators) =>
    validators.every((validator) => validator(value));

  const onInputChange = (ev) => {
    const { value, name } = ev.target;

    if (name === 'username') {
      const copyControl = { ...usernameControl };
      copyControl.value = value;
      copyControl.valid = validate(value, usernameControl.validators);
      setUsernameControl(copyControl);
    } else if (name === 'password') {
      const copyControl = { ...passwordControl };
      copyControl.value = value;
      copyControl.valid = validate(value, passwordControl.validators);
      setPasswordControl(copyControl);
    }
  };

  const handleSubmit = (ev) => {
    ev.preventDefault();

    fetch(`${BASE_URL}/auth/signin`, {
      method: 'POST',
      body: JSON.stringify({
        username: usernameControl.value,
        password: passwordControl.value,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (!data.token) {
          throw new Error(data.error);
        }

        // eslint-disable-next-line react/prop-types
        localStorage.setItem('token', data.token);
        setLoginState({
          isLoggedIn: !!extractUser(data.token),
          user: extractUser(data.token),
          URL: assignURL(extractUser(data.token)),
        });

        if (extractUser(data.token).role === 'teacher') {
          props.history.push('/teacher');
        } else {
          dispatch(addActiveQuiz(extractUser(data.token).activeQuiz));
          props.history.push('/student');
        }
      })
      .catch((error) => Swal.fire(`${error}`, 'Please try again!', 'error'));
  };

  return (
    <div className='hero-container'>
      <img className={'wallpaper'} src={HeroImage2} alt='HeroImage' />

      <div className='container1'>
     
        <Form>
          <h1>Login</h1>
          <br></br>
          <Form.Group controlId='formBasicUsername'>
            <h3>Username</h3>
            <div className='input'>
              <div>
                <Form.Control
                  className='mr-sm-5'
                  value={usernameControl.value}
                  onChange={onInputChange}
                  name='username'
                  type='text'
                  placeholder='Enter Username'
                  style={
                    !usernameControl.valid
                      ? { border: '1px solid #8B0000' }
                      : { border: '2px solid #000080' }
                  }
                />
              </div>
            </div>
            <Form.Text className='text-muted'></Form.Text>
          </Form.Group>

          <Form.Group controlId='formBasicPassword'>
            <h3>Password</h3>
            <div className='input'>
              <div>
                <Form.Control
                  className='mr-sm-5'
                  value={passwordControl.value}
                  onChange={onInputChange}
                  name='password'
                  type='password'
                  placeholder='Password'
                  style={
                    !passwordControl.valid
                      ? { border: '1px solid #8B0000' }
                      : { border: '2px solid #000080' }
                  }
                />
              </div>
            </div>
          </Form.Group>

          <button
            className='btn btn-primary'
            disabled={!usernameControl.valid || !passwordControl.valid}
            onClick={(e) => {
              handleSubmit(e);
            }}
          >
            Login
          </button>
          <br />
          <br />
          <p>Don't have an Account?</p>
          <NavLink to='/signup'>
            <button className='btn btn-dark'>Sign-up Here</button>
          </NavLink>
        </Form>
      </div>
    </div>
  );
};

export default withRouter(LogInPage);
