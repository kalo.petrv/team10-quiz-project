import React, { useState } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import Swal from 'sweetalert2';
import './LogInPage.css';
import { BASE_URL } from '../../common/constants';
import HeroImage2 from '../Pages/Home/HeroSection/HeroImage/quiz2.jpg';

const required = (value) => value.trim().length >= 1;
const minLen = (len) => (value) => value.trim().length >= len;
const maxLen = (len) => (value) => value.trim().length <= len;
const samePassword = () => (pass, ref) => pass === ref;
const regex = (pattern) => (value) => pattern.test(value);

const RegisterPage = (props) => {
  const [usernameControl, setUsernameControl] = useState({
    value: '',
    valid: false,
    validators: [required, minLen(5), maxLen(20)],
  });
  const [firstNameControl, setfirstNameControl] = useState({
    value: '',
    valid: false,
    validators: [required, minLen(3), maxLen(25)],
  });
  const [lastNameControl, setlastNameControl] = useState({
    value: '',
    valid: false,
    validators: [required, minLen(3), maxLen(25)],
  });
  const [passwordControl, setPasswordControl] = useState({
    value: '',
    valid: false,
    validators: [required, minLen(5), maxLen(30)],
  });
  const [passwordControlCheck, setPasswordControlCheck] = useState({
    value: '',
    valid: false,
    validators: [samePassword()],
  });

  const validate = (value, validators) =>
    validators.every((validator) => validator(value));

  const validatePass = (value, value2, validators) =>
    validators.every((validator) => validator(value, value2));

  const onInputChange = (ev) => {
    const { value, name } = ev.target;

    if (name === 'username') {
      const copyControl = { ...usernameControl };
      copyControl.value = value;
      copyControl.valid = validate(value, usernameControl.validators);
      setUsernameControl(copyControl);
    } else if (name === 'firstName') {
      const copyControl = { ...firstNameControl };
      copyControl.value = value;
      copyControl.valid = validate(value, firstNameControl.validators);
      setfirstNameControl(copyControl);
    } else if (name === 'lastName') {
      const copyControl = { ...lastNameControl };
      copyControl.value = value;
      copyControl.valid = validate(value, lastNameControl.validators);
      setlastNameControl(copyControl);
    } else if (name === 'password') {
      const copyControl = { ...passwordControl };
      copyControl.value = value;
      copyControl.valid = validate(value, passwordControl.validators);
      setPasswordControl(copyControl);
    } else if (name === 'confirmPassword') {
      const copyControl = { ...passwordControlCheck };
      copyControl.value = value;
      copyControl.valid = validatePass(
        value,
        passwordControl.value,
        passwordControlCheck.validators
      );
      setPasswordControlCheck(copyControl);
    }
  };
  const path = props.match.path.includes('teacher')
    ? `${BASE_URL}/auth/register/only-for-teachers`
    : `${BASE_URL}/auth/signup`;

  const handleRegister = (ev) => {
    ev.preventDefault();

    fetch(path, {
      method: 'POST',
      body: JSON.stringify({
        username: usernameControl.value,
        password: passwordControl.value,
        firstName: firstNameControl.value,
        lastName: lastNameControl.value,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.error) {
          Swal.fire(`${data.error}`, 'Try Again', 'error');
        } else {
          Swal.fire('Successful Registration!', '', 'success');
          props.history.push('/signin');
        }
      })
      .catch((error) => Swal.fire(`${error}`, '', 'error'));
  };

  return (
    <div>
    <div className='hero-container'>
      <img className={'wallpaper'} src={HeroImage2} alt='HeroImage' />
      <div className='container2'>
        <div>
          <Form>
            <h1>Sign Up</h1>
            <br></br>
            <Form.Group controlId='formBasicUsername'>
              <h4>Username</h4>
              <div className='input'>
                <div>
                  <Form.Control
                    className='mr-sm-5'
                    value={usernameControl.value}
                    onChange={onInputChange}
                    name='username'
                    type='text'
                    placeholder='Enter Username'
                    style={
                      !usernameControl.valid
                        ? { border: '1px solid #8B0000' }
                        : { border: '2px solid #000080' }
                    }
                  />
                </div>
              </div>
            </Form.Group>

            <Form.Group controlId='formBasicUsername'>
              <h4>First and Last Name</h4>
              <div className='input'>
                <div>
                  <Form.Control
                    className='mr-sm-5'
                    value={firstNameControl.value}
                    onChange={onInputChange}
                    name='firstName'
                    type='text'
                    placeholder='Enter First Name'
                    style={
                      !firstNameControl.valid
                        ? { border: '1px solid #8B0000' }
                        : { border: '2px solid #000080' }
                    }
                  />
                </div>
              </div>
            </Form.Group>

            <Form.Group controlId='formBasicUsername'>
              {/* <h4>Last Name</h4> */}
              <div className='input'>
                <div>
                  <Form.Control
                    className='mr-sm-5'
                    value={lastNameControl.value}
                    onChange={onInputChange}
                    name='lastName'
                    type='text'
                    placeholder='Enter Last Name'
                    style={
                      !lastNameControl.valid
                        ? { border: '1px solid #8B0000' }
                        : { border: '2px solid #000080' }
                    }
                  />
                </div>
              </div>
            </Form.Group>

            <Form.Group controlId='formBasicPassword'>
              <h4>Password</h4>
              <div className='input'>
                <div>
                  <Form.Control
                    className='mr-sm-5'
                    value={passwordControl.value}
                    onChange={onInputChange}
                    name='password'
                    type='password'
                    placeholder='Enter Password'
                    style={
                      !passwordControl.valid
                        ? { border: '1px solid #8B0000' }
                        : { border: '2px solid #000080' }
                    }
                  />
                </div>
              </div>
            </Form.Group>

            <Form.Group controlId='formBasicPassword'>
              {/* <h4>Confirm Password</h4> */}
              <div className='input'>
                <div>
                  <Form.Control
                    className='mr-sm-5'
                    value={passwordControlCheck.value}
                    onChange={onInputChange}
                    type='password'
                    name='confirmPassword'
                    placeholder='Confirm Password'
                    style={
                      !passwordControlCheck.valid
                        ? { border: '1px solid #8B0000' }
                        : { border: '2px solid #000080' }
                    }
                  />
                </div>
              </div>
            </Form.Group>
            {/* 
          {!passwordControlCheck.valid && <div>Passwords don't match</div>} */}

            <button
              className='btn btn-primary'
              disabled={
                !usernameControl.valid ||
                !passwordControl.valid ||
                !passwordControlCheck.valid
              }
              onClick={(e) => {
                handleRegister(e);
              }}
            >
              Sign Up
            </button>
            <br></br>
            <br></br>
            <p>Already Have an Account?</p>
            <NavLink to='/signin'>
              <button class='btn btn-dark'>Proceed to Login</button>
            </NavLink>
          </Form>
        </div>
      </div>
    </div>
    </div>
  );
};

export default withRouter(RegisterPage);
