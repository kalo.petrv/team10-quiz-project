import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup'

const SingleQuiz = ({ quiz }) => {

  return (
    <ListGroup.Item className='single-quiz' key={quiz.quiz}>
      <h4>{quiz.quiz_name}</h4>
      <h5>Total Score: <b>{quiz.totalScore}</b>  Possible Score: <b>{quiz.possiblePoints}</b></h5>
    </ListGroup.Item >
  );
};

export default SingleQuiz;