import React from 'react';
import moment from 'moment';

const FullHistory = ({ quizzes }) => {
    let score = 0;
    return (
        <table class="table table-bordered table-dark" style={{ maxWidth: '1000px', margin: 'auto' }}>
            <thead>
                <tr class="bg-info">
                    <th scope="col">Date</th>
                    <th scope="col">Name</th>
                    <th scope="col">My Score</th>
                    <th scope="col">Percent</th>
                </tr>
            </thead>
            <tbody>
                {quizzes.length !== 0 ?
                    quizzes.map((quiz) => {
                        score += quiz.totalScore;
                        return <tr>
                            <th scope="row">{moment(new Date(quiz.date_taken)).format('lll')}</th>
                            <td>{quiz.quiz_name}</td>
                            <td>{quiz.totalScore}/{quiz.possiblePoints}</td>
                            <td>{Math.round(quiz.totalScore / quiz.possiblePoints * 100)}%</td>
                        </tr>
                    })
                    : <tr>
                        <td colspan="4">Empty</td>
                    </tr>}
                <tr>
                    <td colspan="4">Total Score: {score}</td>
                </tr>
            </tbody>
        </table >
    );
};

export default FullHistory