import React from 'react';
import moment from 'moment';

const HistoryByCategory = ({ quizzes }) => {
    return (

        <>
         <table class="table table-hover table-dark">
            <thead class="bg-info">
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Name</th>
                    <th scope="col">My Score</th>
                    <th scope="col">Possible Score</th>
                </tr>
            </thead>
            <tbody>
                {quizzes.length !== 0 ?
                    quizzes.map((quiz) => {
                        return <tr>
                            <th scope="row">{moment(new Date(quiz.date_taken)).format('lll')}</th>
                            <td>{quiz.quiz_name}</td>
                            <td>{quiz.totalScore}</td>
                            <td>{quiz.possiblePoints}</td>
                        </tr>
                    })

                    : <tr>
                        <td colspan="4">Empty</td>
                    </tr>}
            </tbody>
        </table>
        </>
    );
};

export default HistoryByCategory