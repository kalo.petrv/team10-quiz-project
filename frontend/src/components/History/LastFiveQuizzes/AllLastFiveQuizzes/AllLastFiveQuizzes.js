import React from 'react';
import moment from 'moment';

const AllLastFiveQuizzes = ({ quizzes }) => {
    return (
        <>
            <table class="table table-hover table-dark">
                <thead class="bg-info">
                    <tr>
                        <th scope="col">Date</th>
                        <th scope="col">Name</th>
                        <th scope="col">My Score</th>
                        <th scope="col">Percent</th>
                    </tr>
                </thead>
                <tbody>
                    {quizzes.length !== 0 ?
                        quizzes.map((quiz) => {
                            return <tr key={quiz.quiz_id}>
                                <th scope="row">{moment(new Date(quiz.date_taken)).format('lll')}</th>
                                <td>{quiz.quiz_name}</td>
                                <td>{quiz.totalScore}/{quiz.possiblePoints}</td>
                                <td>{Math.round(quiz.totalScore / quiz.possiblePoints * 100)}%</td>
                            </tr>
                        })

                        : <tr>
                            <td colspan="4">No Quizzes Solved</td>
                        </tr>}
                </tbody>
            </table>
        </>
    );
};

export default AllLastFiveQuizzes