import React, { useContext, useEffect, useState } from 'react';
import './Header.css';
import { withRouter } from 'react-router-dom';
import AuthContext from '../../AuthContext/AuthContext';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import httpProvider from '../../../providers/httpProvider';
import UploadProfilePictureContainer from './../../../containers/UploadProfilePictureContainer';
import './../../Leaderboard/SingleUser/SingleUser-avatar.css';
import { BASE_URL } from '../../../common/constants';

const Header = (props) => {
  const { isLoggedIn, user, setLoginState } = useContext(AuthContext);
  const [avatar, setAvatar] = useState('');

  useEffect(() => {
    httpProvider.get(`${BASE_URL}/auth/info`).then((data) => {
      setAvatar(data.avatar);
    });
  }, []);

  const handleLogOut = (e) => {
    e.preventDefault();
    httpProvider.post(`${BASE_URL}/auth/signout`, {});
    setLoginState(false);
    localStorage.removeItem('token');
    props.history.push('/home');
    window.location.reload();
  };

  const handleLogIn = () => {
    props.history.push('/signin');
  };

  return (
    <div className='Header'>
      <Navbar bg='dark' variant='dark'>
        <Nav className='mr-auto'>
          {isLoggedIn ? (
            <button
              style={{ color: 'silver' }}
              className='btn btn-dark'
              onClick={() =>
                props.history.push(
                  `${user.role.includes('teacher') ? '/teacher' : '/student'}`
                )
              }
            >
              <img src='./icon.png' style={{ height: '30px', width: '30px' }} />{' '}
              Dashboard
            </button>
          ) : (
            <Nav.Link href='/home'>
              <img
                src='./icon.png'
          
                style={{ height: '30px', width: '30px', color: 'silver' }}
              />{' '}
              QuizApp
            </Nav.Link>
          )}
        </Nav>

        <Nav>
          <Form inline>
            {isLoggedIn ? (
              <Nav>
                <Navbar.Text>
                  {user.username}
                  <a>&nbsp;</a>
                  <a>&nbsp;</a>
                </Navbar.Text>
                <a>&nbsp;</a>
                <UploadProfilePictureContainer props={props} />
                <a>&nbsp;</a>
                <a>&nbsp;</a>
                <a>&nbsp;</a>
                <button
                  className='btn btn-outline-info'
                  onClick={(e) => {
                    handleLogOut(e);
                  }}
                >
                  Log out
                </button>
              </Nav>
            ) : (
              <button
                className='btn btn-light'
                onClick={(e) => {
                  handleLogIn(e);
                }}
              >
                Log In
              </button>
            )}
          </Form>
        </Nav>
      </Navbar>
    </div>
  );
};

export default withRouter(Header);
