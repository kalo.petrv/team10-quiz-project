import React from 'react';
import AllCategoriesContainer from '../../containers/AllCategoriesContainer';
import LeaderBoardTopUsersContainer from '../../containers/LeaderBoardTopUsersContainer';
import LastFiveQuizzesContainter from '../../containers/LastFiveQuizzesContainter';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { useSelector, useDispatch } from 'react-redux';
import { resetQuizzes, resetHistoryFromCategory } from '../../actions';

const StudentDashboard = (props) => {
  const dispatch = useDispatch();
  const history = useSelector((state) => state.quizzesHistoryByCategory);

  dispatch(resetQuizzes([]));
  dispatch(resetHistoryFromCategory([]));

  return (
    <Container>
      <br></br>
      <h2>Student's Dashboard</h2>
      <br />
      <Row>
        <Col >
          <AllCategoriesContainer />
        </Col>
        <Col>
          <LastFiveQuizzesContainter/>
          <LeaderBoardTopUsersContainer props={props} />
        </Col>
      </Row>
      <br />
    </Container>
  );
};

export default StudentDashboard;
