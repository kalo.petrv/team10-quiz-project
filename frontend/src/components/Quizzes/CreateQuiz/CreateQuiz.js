import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  addAnswer,
  addQuestion,
  chooseCategory,
  getSingleQuiz,
  removeAnswer,
  removeQuestion,
  questionType,
  answerOptionsChange,
} from '../../../actions';
import _ from 'lodash';
import { NavLink } from 'react-router-dom';

const CreateQuiz = ({
  createQuiz,
  quizBody,
  answerBody,
  questionBody,
  categories,
}) => {
  const dispatch = useDispatch();
  const quiz = useSelector((state) => state.quiz);
  dispatch(getSingleQuiz(quizBody));

  const addQuestionToQuiz = (ev, questionBody) => {
    const newQuestion = _.cloneDeep(questionBody);
    newQuestion.id = quiz.quiz_details.questions.length;
    ev.preventDefault();
    dispatch(addQuestion(newQuestion));
  };

  const addAnswerToQuestion = (ev, question_id, answerBody) => {
    const newAnswer = _.cloneDeep(answerBody);
    ev.preventDefault();
    dispatch(addAnswer(question_id, newAnswer));
  };

  const removeAnswerForm = (ev, question_id) => {
    ev.preventDefault();
    dispatch(removeAnswer(question_id));
  };

  const removeQuestionForm = (ev) => {
    ev.preventDefault();
    dispatch(removeQuestion());
  };

  const changeQuestionType = (question_id) => {
    dispatch(questionType(question_id));
  };

  const handleOptionChange = (question_id, answer_id, type) => {
    dispatch(answerOptionsChange(question_id, answer_id, type));
  };

  return !quiz?.quiz_details ? (
    ''
  ) : (
    <div className='create-quiz'>
      <br />
      <h3>Create A New Quiz</h3>
      <div
        style={{
          margin: 'auto',
          maxWidth: '400px',
        }}
      >
        <input
          type='text'
          className='form-control'
          size='50'
          placeholder='Quiz Name...'
          onChange={(e) => (quiz.quiz_details.name = e.target.value)}
        />
      </div>
      <form className='form-group'>
        <h5 htmlFor='categories'>Choose A Category: </h5>
        <select
          name='category-select'
          id='category-select'
          className='form-control'
          style={{
            margin: 'auto',
            maxWidth: '300px',
          }}
          onChange={() => {
            const selected = document.getElementById('category-select');
            dispatch(
              chooseCategory(selected.options[selected.selectedIndex].value)
            );
          }}
        >
          <option value={null} key={null}>
            {'categories'}
          </option>
          {categories.map((c) => {
            return (
              <option value={c.category_name} key={c.category_id}>
                {c.category_name}
              </option>
            );
          })}
        </select>
      </form>
      <hr />
      {!quiz?.quiz_details?.questions
        ? ''
        : quiz.quiz_details.questions.map((q, i) => {
            return (
              <div
                className='question'
                key={i}
                id={i}
                style={{ textAlign: 'justify', marginLeft: '100px' }}
              >
                <form className='form-group'>
                  <div className='row'>
                    <div>
                      <input
                        type='text'
                        className='form-control'
                        name
                        placeholder={`Question ${i + 1}`}
                        style={{
                          minWidth: '400px',
                          maxWidth: '600px',
                        }}
                        onChange={(e) => (q.question = e.target.value)}
                      />{' '}
                    </div>
                    <div>
                      <span>
                        <select
                          name={`points, q${i}`}
                          id={`points, q${i}`}
                          className='custom-select mr-sm-2'
                          style={{
                            maxWidth: '130px',
                          }}
                          onChange={() => {
                            const selected = document.getElementById(
                              `${`points, q${i}`}`
                            );
                            quiz.quiz_details.questions[i].points =
                              selected.options[selected.selectedIndex].value;
                          }}
                        >
                          <option selected key={'choose'}>
                            Choose Points
                          </option>
                          <option value={1} key={'1points'}>
                            {1}
                          </option>
                          <option value={2} key={'2points'}>
                            {2}
                          </option>
                          <option value={3} key={'3points'}>
                            {3}
                          </option>
                          <option value={4} key={'4points'}>
                            {4}
                          </option>
                          <option value={5} key={'5points'}>
                            {5}
                          </option>
                          <option value={6} key={'6points'}>
                            {6}
                          </option>
                        </select>
                      </span>
                    </div>
                  </div>

                  <br />

                  <div class='form-check form-check-inline'>
                    <span className='question-type'>
                      <input
                        type='radio'
                        id={`single-choice${i + 1}`}
                        value='single-choice'
                        name={`question-type${i + 1}`}
                        className='form-check-input'
                        checked={q.type === 0}
                        onChange={() => changeQuestionType(i)}
                      />
                      <label className='form-check-label' defaultChecked={true}>
                        Single Choice
                      </label>
                      {'    '}
                      <input
                        type='radio'
                        id={`multiple-choice${i + 1}`}
                        value='multiple-choice'
                        name={`question-type${i + 1}`}
                        className='form-check-input'
                        checked={q.type === 1}
                        onChange={() => changeQuestionType(i)}
                      />
                      <label className='form-check-label'>
                        Multiple Choice
                      </label>{' '}
                    </span>
                  </div>
                  <br />
                  <br />
                  <div className='answers'>
                    {q.answers.map((a, id) => {
                      return (
                        <div key={id + 1} className='row'>
                          <span>
                            <input
                              type={q.type === 0 ? 'radio' : 'checkbox'}
                              className='form-check-input'
                              id={`${i}, ${id}`}
                              value={a.answer}
                              checked={a.is_correct === 1}
                              name={q}
                              style={{ marginTop: '10px' }}
                              onChange={() => handleOptionChange(i, id, q.type)}
                            />
                          </span>
                          <span> </span>
                          <span>
                            <input
                              type='text'
                              className='form-control'
                              style={{
                                maxWidth: '200px',
                              }}
                              placeholder={`answer ${id + 1}: `}
                              onChange={(e) => (a.answer = e.target.value)}
                            />
                          </span>
                          
                        </div>
                        
                      );
                    })}
                      <br></br>
                    <div className='row'>
                      <button
                        className='btn btn-outline-primary'
                        onClick={(ev) => {
                          addAnswerToQuestion(ev, i, answerBody);
                        }}
                      >
                        Add Answer
                      </button>
                      {'  '}
                      <button
                        className='btn btn-outline-danger'
                        onClick={(ev) => removeAnswerForm(ev, i)}
                      >
                        Remove Last Answer
                      </button>
                    </div>
                  </div>
                </form>
                <hr />
              </div>
            );
          })}
      <div style={{ textAlign: 'justify', marginLeft: '80px' }}>
        <button
          className='btn btn-outline-primary'
          onClick={(e) => addQuestionToQuiz(e, questionBody)}
        >
          Add Question
        </button>{' '}
        <button
          className='btn btn-outline-danger'
          onClick={(ev) => removeQuestionForm(ev)}
        >
          Remove Last Question
        </button>
      </div>
      <button
        className='btn btn-success'
        onClick={(ev) => createQuiz(ev, quiz)}
      >
        Create Quiz
      </button>{' '}
      <NavLink to='/teacher'>
        <button className='btn btn-danger'>Cancel</button>
      </NavLink>
      <br />
      <br />
      <br />
    </div>
  );
};

export default CreateQuiz;
