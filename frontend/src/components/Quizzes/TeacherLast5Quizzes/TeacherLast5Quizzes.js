import React, { useEffect, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getAllQuizzes } from '../../../actions';
import { TEACHER_URL } from '../../../common/constants';
import httpProvider from '../../../providers/httpProvider';
import AuthContext from '../../AuthContext/AuthContext';

const TeacherLast5Quizzes = () => {
  const { user } = useContext(AuthContext);

  const quizzes = useSelector((state) => state.quizzes);
  const dispatch = useDispatch();

  useEffect(() => {
    httpProvider.get(`${TEACHER_URL}/${user.sub}/quizzes/`).then((data) => {

      if (data.error) {
        data.error.toString();
      } else {
        dispatch(getAllQuizzes(data));
      }
    });
  }, []);

  return (
    <div>
      <h3>My Recently Created Quizzes</h3>
      <table className="table table-hover table-dark">
        <thead className="bg-info">
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Category</th>
            <th scope="col">Questions</th>
            <th scope="col">Points</th>
            <th scope="col">Solve</th>
            <th scope="col">View</th>
          </tr>
        </thead>
        <tbody>
          {quizzes.length !== 0 ?
            quizzes.map((quiz) => {
              return <tr key={quiz.id}>
                <td>{quiz.quiz_name}</td>
                <td>{quiz.category_name}</td>
                <td>{quiz.questions}</td>
                <td>{quiz.possiblePoints}</td>
                <td> <NavLink
                  to={`/teacher/categories/${quiz.category_id}/quizzes/${quiz.id}`}
                >
                  <button type="button" className="btn btn-primary btn-sm" >Solve</button>
                </NavLink></td>
                <td> <NavLink
                  to={`/teacher/categories/${quiz.category_id}/quizzes/${quiz.id}/scores`}
                >
                  <button type="button" className="btn btn-primary btn-sm" >View</button>
                </NavLink></td>
              </tr>
            })

            : <tr>
              <td colSpan="6">Empty</td>
            </tr>}
        </tbody>
      </table>
    </div>
  );
};

export default TeacherLast5Quizzes;
