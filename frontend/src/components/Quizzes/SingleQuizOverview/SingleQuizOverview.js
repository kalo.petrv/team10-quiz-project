import React, { useContext, useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import AuthContext from '../../AuthContext/AuthContext';
import { useDispatch } from 'react-redux';
import { addActiveQuiz } from './../../../actions';
import { STUDENT_URL } from '../../../common/constants';
import httpProvider from '../../../providers/httpProvider';
import { withRouter } from 'react-router-dom';
import Swal from 'sweetalert2';

const SingleQuizOverview = ({ history, quiz, category }) => {
  const { user } = useContext(AuthContext);
  const [activeQuiz, setActiveQuiz] = useState(0);

  const path = user && user.role === 'teacher' ? 'teacher' : 'student';

  useEffect(() => {
    httpProvider.get(`${STUDENT_URL}/activequiz`).then((data) => {
      setActiveQuiz(+data.active_quiz);
    });
  }, []);

  const dispatch = useDispatch();

  return (
    <a
      href='#'
      class='list-group-item list-group-item-action flex-column align-items-start'
    >
      <h5 class='mb-1'>{quiz.quiz_name}</h5>
      <p class='text-muted'>{quiz.questions} Questions</p>
      <p class='mb-1'>{quiz.possiblePoints} Points</p>
      {user.role === 'teacher' || activeQuiz === +quiz.id
        ? <button
          class='btn btn-primary'
          onClick={(e) => {
            e.preventDefault()
            history.push(`/${path}/categories/${category.category_id}/quizzes/${quiz.id}`)
            dispatch(addActiveQuiz(quiz.id));
          }}
        >
          Solve
    </button>
        : activeQuiz === 0
          ? <button
            class='btn btn-primary'
            onClick={(e) => {
              e.preventDefault()
              Swal.fire({
                title: 'Are you sure?',
                text: "You have only one chance to take the quiz",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#696969',
                confirmButtonText: 'Confirm'
              }).then((result) => {
                if (result.isConfirmed) {
                  history.push(`/${path}/categories/${category.category_id}/quizzes/${quiz.id}`)
                  dispatch(addActiveQuiz(quiz.id));
                }
              })

            }}
          >
            Solve
      </button>
          : null} {' '}
      {user.role !== 'student' ? (
        <NavLink
          to={`/${path}/categories/${category.category_id}/quizzes/${quiz.id}/scores`}
        >
          <button class='btn btn-dark'>View</button>
        </NavLink>
      ) : null}
    </a>
  );
};

export default withRouter(SingleQuizOverview);
