import React from 'react';

const SolveQuiz = ({
  quiz,
  quizBody,
  answersBody,
  submitSolvedQuiz,
}) => {

  return quiz?.quiz_details ? (
    <div>
      <h2>
        {quiz?.quiz_details[0]?.quiz_name} (category:{' '}
        {quiz?.quiz_details[0]?.category_name})
      </h2>
      <h3>
        Questions: {quiz?.quiz_details[0]?.questions} | Points:{' '}
        {quiz?.quiz_details[0]?.possiblePoints}
      </h3>
      <br />
      <br />

      {quiz?.questions?.map((q, i) => {
        quizBody.answers.push({ ...answersBody });
        quizBody.answers[i].answers_IDS = [];
        return (
          <div key={q.id} style={{ textAlign: 'justify', marginLeft: '100px' }}>
            <h3>
              {q.question} (
              {<span className='text-muted'>points: {q.points}</span>})
            </h3>
            <div>
              <form>
                {q?.answers?.map((a, j) => {
                  a.is_correct = 0;
                  return (
                    <div
                      key={a.id}
                      style={{ textAlign: 'justify', marginLeft: '20px' }}
                    >
                      <h5>
                        <input
                          type={q.type == 0 ? 'radio' : 'checkbox'}
                          id={a.id}
                          value={a.answer}
                          name={q.id}
                          onChange={() => {
                            if (q.type === 0) {
                              quizBody.answers[i].question_id = q.id;
                              quizBody.answers[i].answers_IDS = [];
                              quizBody.answers[i].answers_IDS.push(a.id);
                            } else {
                              quizBody.answers[i].question_id = q.id;
                              if (
                                quizBody.answers[i].answers_IDS.includes(a.id)
                              ) {
                                quizBody.answers[i].answers_IDS.splice(
                                  quizBody.answers[i].answers_IDS.indexOf(a.id),
                                  1
                                );
                              } else {
                                quizBody.answers[i].answers_IDS.push(a.id);
                              }
                            }
                          }}
                        />
                        <label htmlFor={a.id}>
                          <a>&nbsp;</a> <a>&nbsp;</a>
                          {a.answer}
                        </label>
                      </h5>
                    </div>
                  );
                })}
              </form>

              <hr />
              <br />
            </div>
          </div>
        );
      })}
      <button type='submit' className='btn btn-success' onClick={(ev) => { submitSolvedQuiz(ev, quizBody); }}>
        Submit Quiz
      </button>
      <br />
      <br />
    </div>
  ) : (
    ''
  );
};

export default SolveQuiz;
