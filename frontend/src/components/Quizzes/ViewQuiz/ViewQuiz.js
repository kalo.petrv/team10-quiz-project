import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllQuizzes, scoresPerQuiz } from '../../../actions';
import { TEACHER_URL, BASE_URL } from '../../../common/constants';
import httpProvider from '../../../providers/httpProvider';
import moment from 'moment';

const ViewQuiz = (props) => {
  const { id, quizId } = props.match.params;
  const quizzes = useSelector((state) => state.quizzes);
  
  const quiz = quizzes.find((q) => q.id == quizId);
  const dispatch = useDispatch();
  const scores = useSelector((state) => state.leaderboard);

  useEffect(() => {
    httpProvider
      .get(`${TEACHER_URL}/categories/${id}/quizzes/${quizId}/scores`)
      .then((data) => {
        if (data.error) {
          data.error.toString();
        } else {
          dispatch(scoresPerQuiz(data.scores));
        }
      });

    httpProvider.get(`${TEACHER_URL}/categories/${id}/quizzes`).then((data) => {
      if (data.error) {
      } else {
        dispatch(getAllQuizzes(data));
      }
    });
  }, []);

  return !quiz ? (
    ''
  ) : (
      <div>
        <br></br>
        <br></br>
        <br></br>
        <br></br>
        <button
          className='btn btn-secondary'
          onClick={() => props.history.goBack()}
        >
          Go Back
      </button>
        <br />
        <br />

        <h1>
          {quiz.quiz_name} (category: {quiz.category_name})
      </h1>
        <h2>
          <br />
        Questions: {quiz.questions} | Points: {quiz.possiblePoints}{' '}
        </h2>
        <br />

        <br />
        <table
          class='table table-bordered table-dark'
          style={{ maxWidth: '1000px', margin: 'auto' }}
        >
          <thead>
            <tr class='bg-info'>
              <th scope='col'>Avatar</th>
              <th scope='col'>Student</th>
              <th scope='col'>Full Name</th>
              <th scope='col'>Score</th>
              <th scope='col'>Percent</th>
              <th scope='col'>Date Taken</th>
            </tr>
          </thead>
          <tbody>
            {scores.length !== 0 ? (
              scores.map((s) => {
                return (
                  <tr>
                    <td>
                      {' '}
                      <img
                        class='ui avatar image'
                        src={`${BASE_URL}/public/${s.avatar}`}
                      />
                    </td>
                    <th scope='row'> {s.username}</th>

                    <td>
                      {s.firstname} {s.lastname}
                    </td>
                    <td>
                      {s.score}/{quiz.possiblePoints}
                    </td>
                    <td>{Math.round(s.score / quiz.possiblePoints * 100)}%</td>

                    <td>{moment(new Date(s.date_taken)).format('lll')}</td>
                  </tr>
                );
              })
            ) : (
                <tr>
                  <td colspan='4'>No Students have solved the quiz yet</td>
                </tr>
              )}
            <tr></tr>
          </tbody>
        </table>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
      </div>
    );
};

export default ViewQuiz;
