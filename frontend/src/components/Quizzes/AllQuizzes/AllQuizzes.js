import React from 'react'
import SingleQuizOverview from '../SingleQuizOverview/SingleQuizOverview'

const AllQuizzes = ({ quizzes, category }) => {

    return (
        <div class="list-group">
            {quizzes.map((quiz) => {
                return <SingleQuizOverview quiz={quiz} key={quiz.id} category={category} />
            })}
        </div>
    )
}

export default AllQuizzes
