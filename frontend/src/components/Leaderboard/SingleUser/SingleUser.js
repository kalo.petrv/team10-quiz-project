import React from 'react';
import './SingleUser-avatar.css';
import 'semantic-ui-css/semantic.min.css'
import { BASE_URL } from '../../../common/constants';

const SingleUser = ({ user }) => {
  return (
    <div class="item">
      <img class="ui avatar image" src={`${BASE_URL}public/${user.avatar}`} />
      <div class="content">
        <a class="header">{user.username}</a>
        <div class="description">Total scores: <a><b>{user.total_score}</b></a></div>
      </div>
    </div>
  );
};

export default SingleUser;
