import React, { useContext } from 'react';
import 'semantic-ui-css/semantic.min.css';
import './SingleUser/SingleUser-avatar.css';
import AuthContext from '../AuthContext/AuthContext';
import { BASE_URL } from '../../common/constants';

const LeaderBoardView = ({ users }) => {
  const { user } = useContext(AuthContext);

  let place = 0;
  return (
    <table
      class='table table-hover table-dark'
      style={{ maxWidth: '800px', margin: 'auto' }}
    >
      <thead class='bg-info'>
        <tr>
          <th scope='col'>Place</th>
          <th scope='col'>Avatar</th>
          <th scope='col'>Username</th>
          <th scope='col'>Full Name</th>
          <th scope='col'>Score</th>
        </tr>
      </thead>
      <tbody>
        {users.length !== 0 ? (
          users.map((u) => {
            return <tr style={{ backgroundColor: u.user_id === user.sub ? 'green' : '' }} key={u.user_id}>
              <td>#{++place}</td>
              <td>
                <img
                  class='ui avatar image'
                  src={`${BASE_URL}/public/${u.avatar}`}
                />
              </td>
              <td>{u.username}</td>
              <td>
                {u.firstname} {u.lastname}
              </td>
              <td>{u.total_score}</td>
            </tr>

          })
        ) : (
            <tr>
              <td colspan='4'>Empty</td>
            </tr>
          )}
      </tbody>
    </table>
  );
};

export default LeaderBoardView;
