import React, { useState } from 'react';

const CreateCategory = ({createCategory}) => {
  const [categoryText, updateCategoryText] = useState('');
  return (
    <div>
      <form>
        <h2>Add a New Category</h2>
        <input
          type='text'
          className="form-control"
          aria-label="Default"
          aria-describedby="inputGroup-sizing-default"
          id='create-category-text'
          placeholder='New Category...'
          value={categoryText}
          style={{ maxWidth: '400px', margin: 'auto' }}
          onChange={(e) => {
            updateCategoryText(e.target.value);
          }}
        />
        <br></br>
        <button
          type="button" className="btn btn-primary btn-lg"
          type='submit'
          onClick={(e) => {
            createCategory(e, categoryText);
            updateCategoryText('');
          }}
        >
          Add
        </button>
      </form>
    </div>
  );
};

export default CreateCategory;
