import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import AuthContext from '../../AuthContext/AuthContext';
import useStyles from './AllCategories-Style.js'
import { BASE_URL } from '../../../common/constants';

const AllCategories = ({ categories }) => {
  const { user } = useContext(AuthContext);

  const path = user && user.role === 'teacher' ? 'teacher' : 'student';
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <GridList cellHeight={180} className={classes.gridList}>
        {categories.map((category) => {
          return (
            <GridListTile key={category.category_id}>
              <img src={`${BASE_URL}/public/${category.category_photo}`} />
              <NavLink
                to={`/${path}/categories/${category.category_id}/quizzes`}
                category_name={category.category_name}
              >
                <GridListTileBar
                  title={category.category_name}
                />
              </NavLink>
            </GridListTile>
          )
        })}
      </GridList>
    </div >
  );
};

export default AllCategories;
