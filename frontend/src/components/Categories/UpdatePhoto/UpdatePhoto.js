import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Swal from 'sweetalert2';
import { getSingleCategory } from '../../../actions';
import { TEACHER_URL } from '../../../common/constants';
import httpProvider from '../../../providers/httpProvider';

const UpdatePhoto = ({ categoryId }) => {
  const [file, setFile] = useState(null);
  const token = localStorage.getItem('token');

  const category = useSelector((state) => state.category);
  const dispatch = useDispatch();

  const upload = () => {
    if (file) {
      const formData = new FormData();
      formData.set('photo', file);
     
      if (file.name.substr(file.name.length - 4) !== '.jpg' && file.name.substr(file.name.length - 4) !== '.png') {
        Swal.fire({
          title: `Please profide an image file with the extention of jpg/png`,
          icon: 'eror',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'OK',
        });
      } else {
        fetch(`${TEACHER_URL}/${categoryId}/photo`, {
          method: 'POST',
          headers: {
            Authorization: `Bearer ${token}`,
          },
          body: formData,
        })
          .then((r) => r.json())
          .then((data) => {
            if (data.error) {
              Swal.fire(`Photo not updated`, 'Try Again', 'error')
            } else {
              (() => {
                httpProvider.get(`${TEACHER_URL}/categories`).then((data) => {
                  if (data.error) {
                  } else {
                    const obj = data.filter((c) => c.category_id == categoryId);
                    dispatch(getSingleCategory(obj[0]));
                  }
                });
                Swal.fire({
                  title: `Photo is updated successfully`,
                  icon: 'info',
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'OK',
                });
              })();
            }
          });
      }
    }
  };

  return (
    <div>
      <br />
      <h3>Update Category Photo</h3>
      <br />
      <form>
        <div
          className='row'
          style={{
            margin: 'auto',
            maxWidth: '150px',
          }}
        >
          <input
            className='form-control-file'
            type='file'
            accept='image/*'
            onChange={(e) => setFile(e.target.files[0])}
          />
          <br />
          <br />
          <button
            type='button'
            
            className='btn btn-primary'
            style={{
              margin: 'auto',
              maxWidth: '100px',
            }}
            onClick={upload}
          >
            Upload
          </button>
        </div>
      </form>
    </div>
  );
};

export default UpdatePhoto;
