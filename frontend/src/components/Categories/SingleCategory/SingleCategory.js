import React from 'react';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import { BASE_URL } from '../../../common/constants';

const SingleCategory = ({ category }) => { 
  return (
    <>
        <img className={'category-photo'} src={`${BASE_URL}/public/${category.category_photo}`} />
        <GridListTileBar
          title={category.category_name}
        />
    </>
  );
};

export default SingleCategory;
