import { createContext } from 'react';
import jwtDecode from 'jwt-decode';
import { STUDENT_URL, TEACHER_URL } from '../../common/constants';

export const getToken = () => localStorage.getItem('token') || '';

export const extractUser = (token) => {
  try {
    return jwtDecode(token);
  } catch {
    return null;
  }
};

export const assignURL = (user) => {
  if (user && user.role === 'teacher') {
    return TEACHER_URL;
  } else {
    return STUDENT_URL;
  }
}


const AuthContext = createContext({
  isLoggedIn: false,
  user: null,
  URL: '',
  setLoginState: () => { },
});

export default AuthContext;
