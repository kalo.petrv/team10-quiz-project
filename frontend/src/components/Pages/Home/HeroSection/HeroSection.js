import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import './HeroSection.css';
import homeImahe from './HeroImage/18915856.jpg';
import HeroImage2 from './HeroImage/quiz2.jpg';
import { withRouter } from 'react-router-dom';
import AuthContext from '../../../AuthContext/AuthContext';

const HeroSection = ({ history }) => {
  const { user, isLoggedIn } = useContext(AuthContext);
  const path = user && user.role === 'teacher' ? 'teacher' : 'student';

  const handleRegisterButton = () => {
    history.push('/signup');
  };

  const handeDashboard = () => {
    history.push(`/${path}`);
  };

  return (
    <div className='hero-container'>
      <img className={'wallpaper'} src={HeroImage2} alt='HeroImage' />
      <div className='containersr'>
        <div>
          <h2>Join and Learn With Us!</h2>
          <img className={'home-image'} src={homeImahe}></img>
          <br></br>
          <br></br>

            <div>
            {isLoggedIn ? (
              <button
                className='btn btn-dark'
                to='/signup'
                buttonStyle='btn--outline'
                buttonSize='btn--large'
                onClick={handeDashboard}
              >
                Go Back to Dashboard
              </button>
            ) : (
              <div>
                <button
                  className='btn btn-dark'
                  onClick={() => history.push('/signin')}
                >
                  Login
                </button>
                <br/>
                <br/>
                <button
                  className='btn btn-dark'
                  to='/signup'
                  buttonStyle='btn--outline'
                  buttonSize='btn--large'
                  onClick={handleRegisterButton}
                >
                  Create your account
                </button>
              </div>
            )}
          </div>
          <br />
          <br />
          <br />
          <br />
          <br />
        </div>
      </div>
    </div>
  );
};

HeroSection.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(HeroSection);
