import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import './AppError.css';
import httpProvider from '../../../providers/httpProvider';
import AuthContext from '../../AuthContext/AuthContext';
import { BASE_URL } from '../../../common/constants';

const AppError = ({ error, props }) => {
  const { setLoginState } = useContext(AuthContext);

  const handleLogOut = () => {
    httpProvider
      .post(`${BASE_URL}/auth/signout`, {})

    setLoginState(false);
    localStorage.removeItem('token');
    props.history.push('/home');
  };

  return (
    <div className='AppError'>
      <h1>{error}</h1>
      {error.includes('Bad Request') || error.includes('Unexpected token') ? (
        handleLogOut()
      ) : (
        <div>Try Again</div>
      )}
    </div>
  );
};

AppError.propTypes = {
  message: PropTypes.string.isRequired,
};

export default AppError;
